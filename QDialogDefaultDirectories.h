#ifndef QDIALOGDEFAULTDIRECTORIES_H
#define QDIALOGDEFAULTDIRECTORIES_H
#include<QDialog>
#include<QPushButton>
#include<QLineEdit>
#include<QLayout>
#include<QString>
#include<QFileDialog>
#include<QDir>
#include "docTree.h"
#include "TextWizard.h"


class QDialogDefaultDirectories: public QDialog
{
    Q_OBJECT
public:
    QDialogDefaultDirectories();


    QString getDirectory() const;
    void setDirectory(const QString &value);

public slots:
    void addFileDirectories();
    void createNewDir(const QString& dirPath);

signals:
    void sendDir(QDir*);


private:

    QString directory;
    QPushButton* saveButton = new QPushButton(this);
    QPushButton* setDirectoriesButton = new QPushButton(this);
    QLineEdit* fileDirectoriesLine = new QLineEdit(this);


};

#endif // QDIALOGDEFAULTDIRECTORIES_H
