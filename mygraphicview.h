#ifndef MYGRAPHICSVIEW_H
#define MYGRAPHICSVIEW_H
#include <QObject>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QMainWindow>

class MyGraphicsView: public QGraphicsView
{
public:
    MyGraphicsView(QGraphicsScene* scene, QWidget* parent = nullptr);
    MyGraphicsView(QWidget* parent = nullptr);

protected:
    virtual void wheelEvent(QWheelEvent * ev);
};

#endif // MYGRAPHICSVIEW_H
