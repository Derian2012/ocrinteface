#ifndef TREEWIDGETSEARCHUNIQUEENTRYS_H
#define TREEWIDGETSEARCHUNIQUEENTRYS_H
#include <QTreeWidget>
#include "TextWizard.h"


class TreeWidgetSearchUniqueEntrys: public QTreeWidget
{
    Q_OBJECT
public:

    TreeWidgetSearchUniqueEntrys(QWidget* widjet, TextWizard* wizard);
    ~TreeWidgetSearchUniqueEntrys();
    void searchEntry(const QStringList& outText,const QStringList& iPaths, QString& searchedText,TextWizard* wizard);
    void number(const QStringList& outText,const QStringList& iPaths, const QString& searchedText);
    void numberPoint(const QStringList& outText,const QStringList& iPaths, const QString& searchedText);
    void pointNumber(const QStringList& outText,const QStringList& iPaths, const QString& searchedText);
    void numberQuestion(const QStringList& outText,const QStringList& iPaths, const QString& searchedText);
    void questionNumber(const QStringList& outText,const QStringList& iPaths, const QString& searchedText);
    void questionNumberQuestion(const QStringList& outText,const QStringList& iPaths, const QString& searchedText);
    void questionNumberPoint(const QStringList& outText,const QStringList& iPaths, const QString& searchedText);
    void pointNumberQuestion(const QStringList& outText,const QStringList& iPaths, const QString& searchedText);
    void pointNumberPoint(const QStringList& outText,const QStringList& iPaths, const QString& searchedText);
    QString getPath() const;
    void setPath(const QString &path);

private slots:
    void textHighlighting(QTreeWidgetItem* item,int page);

private:
    TextWizard* wiz;
    QString m_path;
    QList<QTreeWidgetItem*> items;
};

#endif // TREEWIDGETSEARCHUNIQUEENTRYS_H
