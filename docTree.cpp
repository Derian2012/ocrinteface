#include "docTree.h"
#include "mainwindow.h"
#include "qmath.h"


DocTree::DocTree(QWidget *widget, TextWizard* wizard):QTreeView (widget),
  wiz(wizard)
{

    this->setModel(currentModel);
   /* currentParent->setData("Doc " +QString::number(parentItems.size()+1),Qt::DisplayRole);
    currentParent->setData(parentItems.size(),Qt::UserRole);
    currentParent->setDropEnabled(true);
    currentParent->setEditable(false);

     currentModel->setItem(parentItems.size(),currentParent);

     parentItems.push_back(currentParent);

     parentIndexes.push_back(m_index);
*/

}

void DocTree::addFile(const QStringList& iPath, QStringList name )
{

    if (!iPath.empty())
    {
    for ( int i =0 ;i < iPath.size();i++) {

     QStandardItem *Item = new QStandardItem();
     Item->setData(iPath[i], Qt::StatusTipRole);
     Item->setData(*m_index,Qt::UserRole);
     Item->setData(name[i],Qt::UserRole+2);
     Item->setText(name[i]);
     Item->setCheckable(true);
     Item->setCheckState( Qt::Checked );
     Item->setEditable(false);
     Item->setDropEnabled(false);
     fileItems.push_back(Item);
     currentParent->setChild(currentParent->rowCount(),Item);
    (*m_index)++;

    }
    currentParent->setData(true,Qt::UserRole+1);
    connect(this,&QListView::clicked, this, &DocTree::changePos);
    }
}

void DocTree::addSingleFile(const QStringList &iPath, QStringList name)
{

}

void DocTree::removeFile(const QString &iPath)
{

     for (int i = 0;i< currentParent->rowCount(); i++)
    {
        if (currentParent->child(i)->data(Qt::StatusTipRole) == iPath){
            currentParent->removeRow(i);
            //delete currentParent->child(i);
            (*m_index)--;
            break;
        }
    }


}

void DocTree::removeParentItem(const QString &path)
{
    for (int i = 0; i < currentParent->rowCount();i++) {
        if (currentParent->child(i)->data(Qt::UserRole+2).toString() == path)
        {
            for (int j =0; j <parentItems.size();j++) {
                if(parentItems[j]->data(Qt::UserRole+2).toString() == path)
               {
                   parentItems.removeAt(j);
                }
            }
            currentParent->removeRow(i);

            //delete currentParent->child(i);
        }
    }


}

QStringList DocTree::getChildren()
{
    QStringList folders;
    for (int i = 0; i < currentParent->rowCount();i++) {
        if(currentParent->child(i)->isDropEnabled()){
        folders.push_back(currentParent->child(i)->data(Qt::UserRole+2).toString());
        }
    }

    return folders;
}

QStringList DocTree::chekedItems()
{
    chekedItemsIndexes.clear();
    QStringList iPath;
    for (int i = 0;i < currentParent->rowCount();i++) {
        if(currentParent->child(i)->checkState() == Qt::Checked ){
       QVariant var = currentParent->child(i)->data(Qt::StatusTipRole);
       iPath.push_back(var.toString());
       //chekedItemsIndexes.push_back(model->item(currentParent->row())->child(i)->data(Qt::UserRole).toInt());
        }
    }
    return iPath;
}

void DocTree::newDocTree(TextWizard* wizard, QString name)
{
    m_index = new int(0);
    parentIndexes.push_back(m_index);
    wiz = wizard;    
    currentParent = new QStandardItem ();
    parentItems.push_back(currentParent);
    currentParent->setData(name,Qt::UserRole+2);
    currentParent->setText(name);
    currentParent->setData(parentItems.size()-1,Qt::UserRole);
    currentParent->setDropEnabled(true);
    currentParent->setEditable(false);
    currentParent->setEnabled(true);
    currentModel->setItem(parentItems.size()-1,currentParent);

}

void DocTree::createParentItem(TextWizard* wizard, QString name, QString parentName)
{

  for(int i = 0; i < parentItems.size();i++ )
  {
      if (parentItems[i]->data(Qt::UserRole+2).toString() == parentName)
      {
          currentParent = parentItems[i];
          m_index= parentIndexes[i];
      }
  }

    QStandardItem *Item = new QStandardItem();
    Item->setData(parentName,Qt::UserRole);
    Item->setData(name,Qt::UserRole+2);
    Item->setText(name);
    Item->setData(parentItems.size(),Qt::UserRole);
    Item->setDropEnabled(true);
    Item->setEditable(false);
    currentParent->setChild(currentParent->rowCount(),Item);
    currentParent = Item;
    parentItems.push_back(currentParent);
    m_index = new int(0);
    parentIndexes.push_back(m_index);
    wiz = wizard;

}

void DocTree::imageChanged(const QString oldiPath, QString newiPath)
{
    QString name = newiPath;
    for (int i =newiPath.size()-1;i > 0  ; i--) {
        if(newiPath[i] == QDir::separator())
        {
         name.remove(0,i+1);
         break;
        }
    }

    QStandardItem* Item;
     for (int i = 0;i< currentParent->rowCount(); i++)
    {
        if (currentParent->child(i)->data(Qt::StatusTipRole) == oldiPath){
        Item = currentParent->child(i);
        Item->setData(newiPath,Qt::StatusTipRole);
        Item->setData(name,Qt::UserRole+2);
        Item->setText(name);
        }
    }




}

void DocTree::changePos(QModelIndex index)
{
    if (currentModel->itemFromIndex(index)->isDropEnabled() == false ){
        if(currentModel->itemFromIndex(index)->parent() == currentParent){
            wiz->centerOnItem(currentModel->itemFromIndex(index)->data(Qt::StatusTipRole).toString());
        }
    }
}

QStandardItemModel *DocTree::getCurrentModel() const
{
    return currentModel;
}

void DocTree::changeWiz(TextWizard *wizard)
{
    wiz = wizard;
}

void DocTree::getAllCheckedItems()
{

}

void DocTree::changeText(QSize size)
{
    for (int i= 0;i < parentItems.size(); i++) {
        QString str = parentItems[i]->data(Qt::UserRole+2).toString();
        QString str1 = "";
        long double count =(size.width()/11)-4;
        if(count >= str.size())
        {
            parentItems[i]->setText(str);
        }
        else{
        str1 = str.remove(0,str.size()-count);
        parentItems[i]->setText("..."+str1);
        }
    }
}

QList<QStandardItem *> DocTree::getParentItems() const
{
    return parentItems;
}

int DocTree::getIndex() const
{
    return *m_index;
}

void DocTree::setIndex(int index)
{
    *m_index = index;
}

void DocTree::changeParentItem(int index)
{
    currentParent = parentItems[index];
    m_index = parentIndexes[index];
}

void DocTree::changeParentItem(QString directoryPath)
{
    for (int i = 0;i<parentItems.size();i++)
    {
        if(parentItems[i]->data(Qt::UserRole+2).toString() == directoryPath)
        {
            currentParent = parentItems[i];
        }
    }
}

bool DocTree::parentItemIsExist(QString directoryPath)
{
    for (int i = 0;i<parentItems.size();i++)
    {
        if(parentItems[i]->data(Qt::UserRole+2).toString() == directoryPath)
        {
            return true;
        }
    }
    return false;

}

QList<int> DocTree::getChekedItemsIndexes()
{
    chekedItemsIndexes.clear();
    QStringList iPath;
    for (int i =0;i<currentParent->rowCount();i++) {
        if(currentParent->child(i)->checkState() == Qt::Checked  ){
       chekedItemsIndexes.push_back(currentParent->child(i)->data(Qt::UserRole).toInt());
       currentParent->child(i)->setCheckState(Qt::Unchecked);
       currentParent->child(i)->setCheckable(false);
       currentParent->child(i)->setBackground(QBrush(Qt::gray));
        }
    }
    return chekedItemsIndexes;
}
