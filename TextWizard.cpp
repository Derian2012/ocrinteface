#include "TextWizard.h"
#include <QGraphicsScene>
#include <QPixmap>
#include <QDir>


TextWizard::TextWizard()
{


}

TextWizard::TextWizard(QMainWindow* window, MyGraphicsView* mainView, QProgressBar* progressBar):QGraphicsScene (window),
    view(mainView),
    prBar(progressBar)
{
    view->setScene(this);
    view->setBackgroundBrush(QBrush(Qt::gray,Qt::SolidPattern));
    view->setMinimumWidth(600);
    window->setCentralWidget(view);
    thread = new QThread(this);
    connect(this,&TextWizard::destroyed,thread,&QThread::quit);
    connect(this,&TextWizard::recognize,currentOcr,&OCR::recognize);
    connect(currentOcr,&OCR::recognizeEnd,this,&TextWizard::addTextEditInWizard);
    connect(currentOcr,&OCR::sendProgress,progressBar,&QProgressBar::setValue);

    currentOcr->moveToThread(thread);
    thread->start();
}

TextWizard::~TextWizard()
{
    for(int i =0 ; i < lists.size();i++ )
    {
        delete lists[i];
    }
    for(int i =0 ; i < items.size();i++ )
    {
        delete items[i];
    }
    for(int i =0 ; i < imageItems.size();i++ )
    {
        delete imageItems[i];
    }
    for(int i =0 ; i < buttonItems.size();i++ )
    {
        delete buttonItems[i];
    }
    thread->exit();
    delete currentOcr;
}

void TextWizard::createList(const QStringList& iPath, QList<int> indexes)
{
    prBar->setTextVisible(true);
    emit recognize(iPath);
    //indexesOfTextEdit = indexes;
}

QStringList TextWizard::recText() const
{    
    return m_recText;
}

QString TextWizard::recTextAtIndex(int index)
{
    return m_recText[index];
}

void TextWizard::addRecText(const QStringList &recText)
{
    m_recText.append(recText);
}

OCR *TextWizard::getCurrentOcr() const
{
    return currentOcr;
}

void TextWizard::setCurrentOcr(OCR *value)
{
    currentOcr = value;
}

MyGraphicsView *TextWizard::getView() const
{
    return view;
}

void TextWizard::centerOnItem(int index)
{
    view->setScene(this);
    view->centerOn(imageItems[index]);
}

void TextWizard::centerOnItem(QString iPath)
{
    for (int i =0;i < imageItems.size(); i++) {
        if(imageItems[i]->data(1).toString() == iPath ){
        view->setScene(this);
        view->centerOn(imageItems[i]);
}
    }
}

void TextWizard::addImage(const QStringList &iPath)
{
    if(!iPath.empty()){
    if(iPath[0].contains(".pdf"))
    {
     Poppler::Document* document = Poppler::Document::load(iPath[0]);
     for (int i = 0;i<document->numPages(); i++) {
         Poppler::Page* pdfPage = document->page(i);
         QImage image = pdfPage->renderToImage(288,288,-1,-1,-1,-1,Poppler::Page::Rotate0);
         image = currentOcr->perspectImage(image);
         int width = image.width();
         image = image.scaledToWidth(2400);
         double scale =double(width)/double(image.width());
         defaultImages.push_back(image.copy(0,0,image.width(),image.height()));
         QGraphicsItem* imageitem = new QGraphicsPixmapItem(QPixmap::fromImage(image));
         imageitem->setData(1,iPath[0]+QDir::separator() + QString::number(i));
         imageitem->setData(3,scale);
         imageitem->setY(y);
         //imageYcoordinats.push_back(y);
         y += image.height()+30;
         this->addItem(imageitem);
         this->setSceneRect(this->sceneRect().x(),this->sceneRect().y(),this->sceneRect().width(),y);
         imageItems.push_back(imageitem);
     }
    }
    else {

    for(int i = 0; i< iPath.size(); i++)
    {
        files.push_back(iPath[i]);
        QImage image = currentOcr->perspectImage(iPath[i]);
        int width;
        width =image.width();
        image = image.scaledToWidth(2400);
        double scale =double(width)/double(image.width());
        defaultImages.push_back(image.copy(0,0,image.width(),image.height()));
        QGraphicsItem* imageitem = new QGraphicsPixmapItem(QPixmap::fromImage(image));
        imageitem->setData(1,iPath[i]);
        imageitem->setData(3,scale);
        imageitem->setY(y);
       //imageYcoordinats.push_back(y);
        y += image.height()+30;
        this->addItem(imageitem);
        //this->setSceneRect(this->sceneRect().x(),this->sceneRect().y(),this->sceneRect().width(),y);
        imageItems.push_back(imageitem);

    }
    }
    }
}

QString TextWizard::recTextFromPath(const QString iPath)
{
    for (int i = 0;i< items.size();i++) {
        if(items[i]->data(1).toString() == iPath)
        {
          return m_recText[i];
        }

    }
    return "empty";
}

double TextWizard::scaleOfImageFromPath(const QString &iPath)
{
    for (int i = 0;i < imageItems.size();i++) {
        if(imageItems[i]->data(1) == iPath)
        {
            double re = imageItems[i]->data(3).toDouble();
            return  imageItems[i]->data(3).toDouble();
        }
    }
    return 0;
}

int TextWizard::indexOfRecText(const QString iPath)
{
    for (int i = 0;i< items.size();i++) {
        QString str = items[i]->data(1).toString();
        if(items[i]->data(1).toString() == iPath)
        {
          return i;
        }

    }
return -1;
}

QList<QTextEdit *> TextWizard::getLists() const
{
    return lists;
}

void TextWizard::setLists(const QList<QTextEdit *> &value)
{
    lists = value;
}

void TextWizard::sendSignalPush(QGraphicsItem* item)
{
    emit buttonPushed(item);
}

void TextWizard::showHide(QGraphicsItem* item)
{
    if(item->isVisible() == true)
    {
        item->setVisible(false);
    }
    else {
        item->setVisible(true);
    }

}

void TextWizard::addTextEditInWizard(const QStringList& iPaths)
{
    prBar->setTextVisible(false);
    prBar->setValue(0);
    for (int i = 0;i <currentOcr->outText().size() ; i++ ){        
    setWordsData(currentOcr->getTSVTextes()[i],iPaths[i]);
    QPushButton* button = new QPushButton("+++++");
    QTextEdit *ntext = new QTextEdit();
    QString str = currentOcr->outText()[i];
    recognizedFiles.push_back(iPaths[i]);
    m_recText.push_back(str);
    ntext->setText(str);
    ntext->selectAll();
    ntext->setFontPointSize(34);
    ntext->setTextCursor(ntext->textCursor());
    ntext->resize(2400,3200);

    ntext->setAlignment(Qt::AlignVertical_Mask);
    button->resize(300,300);
    QGraphicsItem* buttonItem = this->addWidget(button);
    QGraphicsProxyWidget* item = this->addWidget(ntext);

    item->setData(2,m_recText.size()-1);
    if(isItColum){
    for (int j = 0;j < imageItems.size(); j++) {
        if (imageItems[j]->data(1).toString() == iPaths[i])
        {
            item->setY(imageItems[j]->y());
            item->setData(1,imageItems[j]->data(1));
            buttonItem->setY(imageItems[j]->y());
            buttonItem->setX(imageItems[j]->x() - 400);
            buttonItem->setData(1,imageItems[j]->data(1));
        }
    }
    }else {
        this->setSceneRect(this->sceneRect().x(),this->sceneRect().y(),imageItems[0]->boundingRect().width()*2,y);
        for (int j = 0;j < imageItems.size(); j++) {
            imageItems[i]->setX(this->sceneRect().x());
            if (imageItems[j]->data(1).toString() == iPaths[i])
            {
                item->setY(imageItems[j]->y());
                item->setData(1,imageItems[j]->data(1));
                item->setX(this->sceneRect().x()+item->boundingRect().width()+ 40);
                buttonItem->setY(imageItems[j]->y());
                buttonItem->setX(imageItems[j]->x() - 400);
                buttonItem->setData(1,imageItems[j]->data(1));
                buttonItem->setVisible(false);
            }

    }
 view->centerOn(this->sceneRect().x(),this->sceneRect().y());
}


    buttonItems.push_back(buttonItem);
    lists.push_back(ntext);
    items.push_back(item);
    connect(button,&QPushButton::clicked,[=](){if(item->isVisible() == true)
        {
            item->setVisible(false);
        }
        else {
            item->setVisible(true);
        };});


    }
    inProgress = false;
}

void TextWizard::setInPrigress(bool value)
{
    inProgress = value;
}

void TextWizard::changeViewModel()
{
    if(isItColum)
    {  isItColum = false;
     if (inProgress == false){
        if(!items.empty()){
            this->setSceneRect(this->sceneRect().x(),this->sceneRect().y(),imageItems[0]->boundingRect().width()*2,y);
            for (int i =0;i<imageItems.size();i++) {
                imageItems[i]->setX(this->sceneRect().x());
            }
        for (int i =0;i<items.size();i++) {

            items[i]->setX(this->sceneRect().x()+items[i]->boundingRect().width()+ 40);
            buttonItems[i]->setVisible(false);
        }

        //view->setMinimumWidth(this->sceneRect().width());
        view->centerOn(this->sceneRect().x(),this->sceneRect().y());
        }
     }
    }
    else {
        isItColum = true;
        if (inProgress == false){
        this->setSceneRect(this->sceneRect().x(),this->sceneRect().y(),imageItems[0]->boundingRect().width(),y);
        for (int i = 0; i < items.size(); ++i) {
            for (int j = 0;j < imageItems.size(); j++) {
                imageItems[i]->setX(0);
                if (items[i]->data(1).toString() == imageItems[j]->data(1).toString())
                {
                    items[i]->setX(imageItems[j]->x());
                    buttonItems[i]->setVisible(true);
                }
            }

        }
 //view->setMaximumWidth(items[0]->boundingRect().width());

        }
    }

}

bool TextWizard::setWordsData(QString TSVtext, QString iPath)
{
    for (int i=0 ;i < TSVtext.count("\n"); i++) {
        QString TSVString = TSVtext.section("\n",i,i);
            WordData data;
            data.level =  TSVString.section("\t",0,0);
            data.page_num =  TSVString.section("\t",1,1);
            data.block_num =  TSVString.section("\t",2,2);
            data.par_num =  TSVString.section("\t",3,3);
            data.line_num =  TSVString.section("\t",4,4);
            data.word_num =  TSVString.section("\t",5,5);
            data.top =  TSVString.section("\t",6,6);
            data.left =  TSVString.section("\t",7,7);
            data.width =  TSVString.section("\t",8,8);
            data.height =  TSVString.section("\t",9,9);
            data.conf =  TSVString.section("\t",10,10);
            data.text =  TSVString.section("\t",11,11);
            data.iPath = iPath;
            wordsData.push_back(data);
    }
    return true;

}

QStringList TextWizard::getRecognizedFiles() const
{
    return recognizedFiles;
}

void TextWizard::removeRecognizedFile(int index)
{
    recognizedFiles.removeAt(index);
}

void TextWizard::setRecognizedFiles(const QStringList &value)
{
    recognizedFiles = value;
}

void TextWizard::drawRect(QString text, QString iPath)
{
    for (int i = 0; i <imageItems.size();i++) {
        if(imageItems[i]->data(1).toString() == iPath)
           {
            QImage img = defaultImages[i].copy(0,0,defaultImages[i].width(),defaultImages[i].height());

            for (int j =0; j < wordsData.size(); j++) {
                if(wordsData[j].text == text && wordsData[j].iPath == iPath)
                {

                    int w = int((wordsData[j].width.toInt()/scaleOfImageFromPath(iPath))*1.2);
                    int h = int((wordsData[j].height.toInt()/scaleOfImageFromPath(iPath))*1.3);
                    int x = int(wordsData[j].top.toInt()/scaleOfImageFromPath(iPath) - w*0.05);
                    int y = int(wordsData[j].left.toInt()/scaleOfImageFromPath(iPath)- h*0.05);
                    QPainter painter(&img);
                    QImage im = defaultImages[i].copy(x,y,w,h);
                    im.invertPixels();
                    painter.drawImage(x,y,im);
                }


            }


            QGraphicsItem* imageitem = new QGraphicsPixmapItem(QPixmap::fromImage(img));
            imageitem->setData(1,imageItems[i]->data(1));
            imageitem->setData(3,imageItems[i]->data(3));
            imageitem->setY(imageItems[i]->y());
            this->removeItem(imageItems[i]);
            delete imageItems[i];
            imageItems[i]=imageitem;
            this->addItem(imageitem);
        }
    }
}

QStringList TextWizard::getFiles() const
{
    return files;
}

void TextWizard::changeFiles(int index, QString fileName)
{
    files[index] = fileName;
}

void TextWizard::removeImage(const QString fileName)
{
    for (int i = 0;i < imageItems.size() ; i++) {
        if (imageItems[i]->data(1).toString() == fileName)
        {
            y= imageItems [i]->y();
            for (int j = 0;j <imageItems.size(); j++) {
                if (imageItems[j]->y() > imageItems[i]->y())
                {
                    imageItems[j]->setY(y);

                    y+= imageItems[j]->boundingRect().height() + 30;
                }

            }
            this->removeItem(imageItems[i]);
            imageItems.removeAt(i);
            this->setSceneRect(this->sceneRect().x(),this->sceneRect().y(),this->sceneRect().width(),y);
            break;
        }

    }
    if (!items.empty()){
    for (int i = 0; i < items.size(); ++i) {
        if(items[i]->data(1).toString() == fileName)
        {
         this->removeItem(items[i]);
            this->removeItem(buttonItems[i]);
            recognizedFiles.removeAt(i);
            buttonItems.removeAt(i);
            lists.removeAt(i);
            items.removeAt(i);

        }
        for (int j = 0;j < imageItems.size(); j++) {
            if (items[i]->data(1).toString() == imageItems[j]->data(1).toString())
            {
                items[i]->setY(imageItems[j]->y());
                buttonItems[i]->setY(imageItems[j]->y());
            }
        }

    }
}

}

void TextWizard::removeWordsData(const QString fileName)
{
    for (int i =0; i <wordsData.size();i++) {
        if(wordsData[i].iPath == fileName)
        {
            wordsData.removeAt(i);
        }
    }

}

QString TextWizard::getPath() const
{
    return path;
}

void TextWizard::setPath(const QString &value)
{
    path = value;
}

void TextWizard::addToFiles(const QString path)
{
    files.push_back(path);
}

void TextWizard::removeFile(const QString path)
{
    for (int i = 0; i < files.size(); i++) {
        if( files[i] == path)
        {
            files.removeAt(i);
        }
    }
}

