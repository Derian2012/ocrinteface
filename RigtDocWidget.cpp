#include "RightDocWidget.h"
#include "mainwindow.h"
RDocWidget::RDocWidget(TextWizard* wizard)
{
    wiz = wizard;
    m_path = wiz->getPath();
    this->setHeaderLabels({"Number","Page"});
    connect(wiz,&TextWizard::destroyed,this,&RDocWidget::deleteLater);
    //this->setWidget(currentRigtWgs);

}

void RDocWidget::textHighlighting(QTreeWidgetItem* item, int page)
{
    if(item->text(1) != ""){
    QString _text = item->text(0);
    QString list = item->data(1,Qt::UserRole+1).toString();
    wiz->centerOnItem(list);

    QString str = wiz->recTextFromPath(list);

    int index = item->data(1,Qt::UserRole).toInt();
    int begin = 0;
    int end = 0;

    for (int i = index;i < str.size();i++)
    {
        if(str[i] == " " || str[i] == "\n")
        {
            end = i;
            break;
        }

    }
    for (int i = index;i < str.size();i--)
    {
        if(str[i] == " " || str[i] == "\n")
        {
            begin = i;
            break;
        }

    }

    str.chop(str.size()-end);
    str.remove(0,begin+1);
    wiz->drawRect(str,list);
    QTextCharFormat fmt;
    fmt.setFontPointSize(34);
    QTextCursor cursor = wiz->getLists()[wiz->indexOfRecText(list)]->textCursor();
    cursor.setPosition(begin,QTextCursor::MoveAnchor);
    cursor.setPosition(end,QTextCursor::KeepAnchor);
    cursor.setCharFormat(fmt);
    wiz->getLists()[wiz->indexOfRecText(list)]->setTextCursor(cursor);
    }

}

void RDocWidget::numbersFilter(const QString &number)
{
    if(number != ""){
    for (int i = 0 ; i < topNums.size(); i++) {
        if (topNums[i]->data(0,Qt::DisplayRole).toString().indexOf(number) != 0)
        {
            topNums[i]->setHidden(true);
        }
        else {
             topNums[i]->setHidden(false);
        }


    }
    }
    else {
        for (int i = 0 ;i< topNums.size(); i++) {

            topNums[i]->setHidden(false);

        }

    }

}

QString nameFromDir(QString path)
{
    for (int i = path.size() -1; i> 0; i--)
    {
        if (path[i] == QDir::separator())
        {
            return path.remove(0,i+1);
        }
    }
    return "empty";
}

void RDocWidget::addNumbers(const QStringList str,const QStringList iPaths)
{
    uniquEntry(str,iPaths);
    this->clear();
    nums.clear();
    QString str1,str2;
    for (int i = 0; i<getNumbers().size();i++)
    {


if (getNumbers()[i].inTree == false){
    QTreeWidgetItem* num = new QTreeWidgetItem(this);
              num->setData(0,Qt::DisplayRole,getNumbers()[i].number);
              topNums.push_back(num);
        for (int j = 0;j < getNumbers().size();j ++) {
            if(getNumbers()[i].number == getNumbers()[j].number && getNumbers()[j].inTree == false)
            {

                QTreeWidgetItem* numtr = new QTreeWidgetItem(num);
                numtr->setText(0,getNumbers()[j].number);
                numtr->setData(1,Qt::UserRole,getNumbers()[j].index);
                numtr->setData(1,Qt::UserRole+1,getNumbers()[j].path);
                numtr->setText(1,nameFromDir(getNumbers()[j].path));
                setNumbersInTree(j);
                nums.push_back(numtr);
                connect(this, &QTreeWidget::itemClicked, this, &RDocWidget::textHighlighting);

            }

        }


    }
    }


}

QList<RDocWidget::Numbers> RDocWidget::getNumbers() const
{
    return numbers;
}

void RDocWidget::setNumbersInTree(const int index)
{
    numbers[index].inTree = true;
}

void RDocWidget::setNumbers(const QList<RDocWidget::Numbers> &value)
{
    numbers = value;
}

void RDocWidget::uniquEntry(const QStringList &Text,const QStringList iPaths)
{
    numbers.clear();

    for (int pagenum=0; pagenum < Text.size();pagenum++) {


    for (int i = 0; i <Text[pagenum].size(); ++i) {
        Numbers num;
        if (Text[pagenum][i] >='0' && Text[pagenum][i] <= '9')
        {
            if (Text[pagenum][i+1] >='0' && Text[pagenum][i+1] <= '9')
            {
                num.index = i;
                while(Text[pagenum][i] >='0' && Text[pagenum][i]<= '9')
                {
                    num.number+= Text[pagenum][i];
                    num.path = iPaths[pagenum];
                    i++;
                }
                numbers.push_back(num);

            }
        }

    }

    }

    for (int j =0 ;j<numbers.size()-1;j++){
    for (int i =0 ;i<numbers.size()-1;i++) {
      if (numbers[i].number.size()<numbers[i +1].number.size())
      {
          numbers[i].number.swap(numbers[i+1].number);
          swap(numbers[i].path,numbers[i+1].path);
          swap(numbers[i].index,numbers[i+1].index);
      }
    }
    }

    for (int j =0 ;j<numbers.size()-1;j++){
    for (int i =0 ;i<numbers.size()-1;i++) {
      if (numbers[i].number.size() == numbers[i +1].number.size())
      {
          swapIfBigger(numbers[i],numbers[i +1]);
      }
}
}

}

void RDocWidget::swapIfBigger(RDocWidget::Numbers& num1, RDocWidget::Numbers& num2)
{

    for (int i =0 ; i < num1.number.size() ;i++) {
        int n1 = num1.number[i].digitValue();
        int n2 = num2.number[i].digitValue();
        if(num1.number[i].digitValue() < num2.number[i].digitValue() )
        {
            num1.number.swap(num2.number);
            swap(num1.path,num2.path);
            swap(num1.index,num2.index);
            break;
        }
        else if(num1.number[i].digitValue() > num2.number[i].digitValue()) {
            break;
        }
    }

}

QString RDocWidget::getPath() const
{
return m_path;
}

