#ifndef OCR_H
#define OCR_H
#include <string>
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>
#include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>
#include </home/aleksandr/LibsAndInclude/include/poppler-qt5.h>
#include <QImage>
#include <QObject>
#include <QString>
#include <QFileDialog>
#include <QThread>
using namespace cv;
class OCR: public QObject {
    Q_OBJECT
// коментарий для задания
// коментарий для задания 2 
private:

    QStringList m_outText;
    QStringList m_iPath;
    QStringList TSVTextes;
    void doPerspect(Mat &inputImg, Mat &outputImg);


public:
OCR(QObject* pobj = 0);
QImage perspectImage(QString imagePath);
QImage perspectImage(QImage imagePath);
void openFile();
QStringList iPath() const;
void setIPath(const QStringList &iPath);
 QStringList outText() const;
void setOutText(const  QStringList &outText);
QString outTextatindex(const int index);
QList<Mat> segmentation(Mat image);

QStringList getTSVTextes() const;
void setTSVTextes(const QStringList &value);
bool setWordsData(QString TSVtext);
double angleOfText(Mat image);

public slots:
void recognize(const QStringList& str);

signals:
void recognizeEnd(const QStringList& iPaths);
void sendProgress(int progress);
};

#endif // OCR_H
