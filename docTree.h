#ifndef DOCTREE_H
#define DOCTREE_H
#include <QTreeView>
#include <QListView>
#include <QString>
#include <QStringListModel>
#include <QDockWidget>
#include <QStandardItemModel>
#include <QListWidgetItem>
#include <QSize>
#include <QDir>
#include "TextWizard.h"

class DocTree: public QTreeView
{
    Q_OBJECT
public:
    DocTree(QWidget* widget,TextWizard* wizard);
    void addFile(const QStringList& iPath , QStringList name );
    void addSingleFile(const QStringList& iPath , QStringList name);
    void removeFile(const QString& iPath);
    void removeParentItem(const QString& path);
    QStringList getChildren();
    QStringList chekedItems();
    void newDocTree(TextWizard* wizard, QString name);
    void createParentItem(TextWizard* wizard = nullptr, QString name = "empty", QString parentName = "empty");
    void imageChanged(const QString oldiPath, QString newiPath);

    QList<int> getChekedItemsIndexes();

    int getIndex() const;
    void setIndex(int index);
    void changeParentItem(int index);
    void changeParentItem(QString directoryPath);
    bool parentItemIsExist(QString directoryPath);

    QStandardItemModel *getCurrentModel() const;

    void changeWiz(TextWizard* wizard);

    void textSizeChange();
    void getAllCheckedItems();

    QList<QStandardItem *> getParentItems() const;

public slots:
    void changePos(QModelIndex index);
       void changeText(QSize size);


private:

    TextWizard* wiz;
    QStringList files;
    QList<QStandardItemModel*> m_models;
    QStandardItemModel* currentModel= new QStandardItemModel(this);

    QList<QStandardItem*> parentItems;
    QStandardItem* currentParent = new QStandardItem();
    QList<int> chekedItemsIndexes;
    QList<QStandardItem*> fileItems;


    QList<int*> parentIndexes;
    int* m_index = new int(0);


};

#endif // DOCTREE_H

