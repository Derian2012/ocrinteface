#ifndef RIGTDOCWIDGET_H
#define RIGTDOCWIDGET_H
#include <QDockWidget>
#include <QTreeWidget>
#include "TextWizard.h"
#include "OCR.h"



class RDocWidget : public QTreeWidget
{
    Q_OBJECT
public:
    RDocWidget(TextWizard* wizard);
    struct Numbers{ QString number;QString path;int index; bool inTree =false;};
    void addNumbers(const QStringList str,const QStringList iPaths);
    QList<Numbers> getNumbers() const;
    void setNumbersInTree(const int index);
    void setNumbers(const QList<Numbers> &value);
    void uniquEntry (const QStringList& outText,const QStringList iPaths);
    void newRghtWidget();
    void setCount(Numbers* number, int count);
    void swapIfBigger(Numbers& num1 , Numbers& num2);


    QString getPath() const;

public slots:
    void textHighlighting(QTreeWidgetItem* item,int page);
    void numbersFilter(const QString& number);

private:
    TextWizard* wiz;
    QString m_path;
    QList<QTreeWidget*> rightWgs;
    QList<QTreeWidgetItem*> nums;
    QList<Numbers> numbers;
    QList<QTreeWidgetItem*> topNums;


};

#endif // RIGTDOCWIDGET_H




