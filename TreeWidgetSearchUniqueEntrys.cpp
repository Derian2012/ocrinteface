#include "TreeWidgetSearchUniqueEntrys.h"

TreeWidgetSearchUniqueEntrys::TreeWidgetSearchUniqueEntrys(QWidget* widjet,TextWizard* wizard):
    QTreeWidget (widjet)
{
    wiz = wizard;
    m_path =wiz->getPath();
    this->setHeaderLabel("Searched Text");
    connect(wiz,&TextWizard::destroyed,this,&TreeWidgetSearchUniqueEntrys::deleteLater);
}

TreeWidgetSearchUniqueEntrys::~TreeWidgetSearchUniqueEntrys()
{

}

void TreeWidgetSearchUniqueEntrys::searchEntry(const QStringList &outText, const QStringList &iPaths,  QString &searchedText, TextWizard* wizard)
{
    wiz = wizard;
    this->clear();
    items.clear();
    if(searchedText.startsWith("*"))
    {
        if(searchedText.endsWith("*"))
        {
            searchedText.remove(0,1);
            searchedText.remove(searchedText.size()-1,1);
         pointNumberPoint(outText,iPaths,searchedText);
        }
        else if(searchedText.endsWith("?"))
        {
            searchedText.remove(0,1);
            searchedText.remove(searchedText.size()-1,1);
            pointNumberQuestion(outText,iPaths,searchedText);
        }
        else {
            searchedText.remove(0,1);
            pointNumber(outText,iPaths,searchedText);
        }

    }
    else if(searchedText.startsWith("?"))
    {
        if(searchedText.endsWith("?"))
        {
            searchedText.remove(0,1);
            searchedText.remove(searchedText.size()-1,1);
            questionNumberQuestion(outText,iPaths,searchedText);
        }
        else if(searchedText.endsWith("*"))
        {
            searchedText.remove(0,1);
            searchedText.remove(searchedText.size()-1,1);
            questionNumberPoint(outText,iPaths,searchedText);
        }
        else {
            searchedText.remove(0,1);
            questionNumber(outText,iPaths,searchedText);
        }

    }else if (searchedText.endsWith("*")) {
        searchedText.remove(searchedText.size()-1,1);
        numberPoint(outText,iPaths,searchedText);
        }
    else if(searchedText.endsWith("?")) {
        searchedText.remove(searchedText.size()-1,1);
        numberQuestion(outText,iPaths,searchedText);

    }
    else {
        number(outText,iPaths,searchedText);
    }

}

void TreeWidgetSearchUniqueEntrys::number(const QStringList &outText, const QStringList &iPaths, const QString &searchedText)
{


if(!searchedText.isEmpty()){
    for (int page = 0;page < outText.size(); page++ ) {

        for (int i = 0;i < outText[page].size(); i++) {
            int index = 1;
            if(outText[page].indexOf(" "+searchedText+" ",i) != -1)
            {
                i = outText[page].indexOf(" "+searchedText+" ",i);
                QTreeWidgetItem* text = new QTreeWidgetItem(this);
                items.push_back(text);
                text->setData(0,Qt::DisplayRole,searchedText);
                text->setData(0,Qt::UserRole,iPaths[page]);
                text->setData(0,Qt::UserRole +1 ,i);
                index++;

                connect(this,&QTreeWidget::itemClicked,this,&TreeWidgetSearchUniqueEntrys::textHighlighting);
            }
            else{break;}

        }
        for (int i = 0;i < outText[page].size(); i++) {
            int index = 1;
            if( outText[page].indexOf("\n"+searchedText+"\n",i) != -1 )
            {
                i = outText[page].indexOf("\n"+searchedText+"\n",i);
                QTreeWidgetItem* text = new QTreeWidgetItem(this);
                items.push_back(text);
                text->setData(0,Qt::DisplayRole,searchedText);
                text->setData(0,Qt::UserRole,iPaths[page]);
                text->setData(0,Qt::UserRole +1,i);
                index++;


            }
            else{break;}
        }


        for (int i = 0;i < outText[page].size(); i++) {
            int index = 1;
            if(outText[page].indexOf(" "+searchedText+"\n",i) != -1)
            {
                i = outText[page].indexOf(" "+searchedText+"\n",i);
                QTreeWidgetItem* text = new QTreeWidgetItem(this);
                items.push_back(text);
                text->setData(0,Qt::DisplayRole,searchedText);
                text->setData(0,Qt::UserRole,iPaths[page]);
                text->setData(0,Qt::UserRole +1,i);
                index++;


            }
            else{break;}
        }
        for (int i = 0;i < outText[page].size(); i++) {
            int index = 1;
            if(outText[page].indexOf("\n"+searchedText+" ",i) != -1)
            {
                i = outText[page].indexOf("\n"+searchedText+" ",i);
                QTreeWidgetItem* text = new QTreeWidgetItem(this);
                items.push_back(text);
                text->setData(0,Qt::DisplayRole,searchedText);
                text->setData(0,Qt::UserRole,iPaths[page]);
                text->setData(0,Qt::UserRole +1,i);
                index++;
            }
            else{break;}
        }

    }
}

}

void TreeWidgetSearchUniqueEntrys::numberPoint(const QStringList &outText, const QStringList &iPaths, const QString &searchedText)
{
    if(!searchedText.isEmpty()){
    for (int page = 0;page<outText.size();page++) {

        for (int i = 0;i<outText[page].size();i++) {
            if(outText[page].indexOf(" "+searchedText,i) != -1)
            {
                i = outText[page].indexOf(" "+searchedText,i);

                QString temp = outText[page];
                for (int j = i;j < outText[page].size(); j--)
                {
                    if(outText[page][j] == " " || outText[page][j] == "\n")
                    {
                        QTreeWidgetItem* text = new QTreeWidgetItem(this);
                        items.push_back(text);
                       text->setData(0,Qt::UserRole +1 ,j);
                       temp.remove(0,j+1);
                       temp.remove(temp.indexOf(" "),temp.size() - temp.indexOf(" "));
                       if(temp.indexOf("\n") != -1){
                           temp.remove(temp.indexOf("\n"),temp.size() - temp.indexOf("\n"));
                       }
                       text->setData(0,Qt::DisplayRole,temp);
                       text->setData(0,Qt::UserRole,iPaths[page]);
                       break;

                    }

                }
                connect(this,&QTreeWidget::itemClicked,this,&TreeWidgetSearchUniqueEntrys::textHighlighting);
            }
            else{break;}
        }
        for (int i = 0;i<outText[page].size();i++) {
            if(outText[page].indexOf("\n"+searchedText,i) != -1)
            {
                i = outText[page].indexOf("\n"+searchedText,i);
                QTreeWidgetItem* text = new QTreeWidgetItem(this);

                for (int j = i;j < outText[page].size(); j--)
                {
                    if(outText[page][j] == " " || outText[page][j] == "\n")
                    {
                        items.push_back(text);
                        QString temp = outText[page];
                       text->setData(0,Qt::UserRole +1 ,j);
                       temp.remove(0,j+1);
                       temp.remove(temp.indexOf(" "),temp.size() - temp.indexOf(" "));
                       if(temp.indexOf("\n") != -1){
                           temp.remove(temp.indexOf("\n"),temp.size() - temp.indexOf("\n"));
                       }
                       text->setData(0,Qt::DisplayRole,temp);
                       text->setData(0,Qt::UserRole,iPaths[page]);
                       break;

                    }

                }
                connect(this,&QTreeWidget::itemClicked,this,&TreeWidgetSearchUniqueEntrys::textHighlighting);
            }
            else{break;}
        }


    }
    }
}

void TreeWidgetSearchUniqueEntrys::pointNumber(const QStringList &outText, const QStringList &iPaths, const QString &searchedText)
{
    if(!searchedText.isEmpty()){
    for (int page = 0;page<outText.size();page++) {

        for (int i = 0;i<outText[page].size();i++) {
            if(outText[page].indexOf(searchedText+" ",i) != -1)
            {
                i = outText[page].indexOf(searchedText+" ",i);
                QString temp = outText[page];
                for (int j = i ;j < outText[page].size(); j--)
                {
                    if(outText[page][j] == " " || outText[page][j] == "\n")
                    {
                        QTreeWidgetItem* text = new QTreeWidgetItem(this);
                        items.push_back(text);
                       text->setData(0,Qt::UserRole +1 ,j);
                       temp.remove(0,j+1);
                       temp.remove(temp.indexOf(" "),temp.size() - temp.indexOf(" "));
                       if(temp.indexOf("\n") != -1){
                           temp.remove(temp.indexOf("\n"),temp.size() - temp.indexOf("\n"));
                       }
                       text->setData(0,Qt::DisplayRole,temp);
                       text->setData(0,Qt::UserRole,iPaths[page]);
                       break;

                    }

                }
                connect(this,&QTreeWidget::itemClicked,this,&TreeWidgetSearchUniqueEntrys::textHighlighting);
            }
            else{break;}
        }
        for (int i = 0;i<outText[page].size();i++) {
            if(outText[page].indexOf(searchedText+"\n",i) != -1)
            {
                i = outText[page].indexOf(searchedText+"\n",i);

                QString temp = outText[page];
                for (int j = i;j < outText[page].size(); j--)
                {
                    if(outText[page][j] == " " || outText[page][j] == "\n")
                    {
                        QTreeWidgetItem* text = new QTreeWidgetItem(this);
                        items.push_back(text);
                       text->setData(0,Qt::UserRole +1 ,j);
                       temp.remove(0,j+1);
                       temp.remove(temp.indexOf(" "),temp.size() - temp.indexOf(" "));
                       if(temp.indexOf("\n") != -1){
                           temp.remove(temp.indexOf("\n"),temp.size() - temp.indexOf("\n"));
                       }
                       text->setData(0,Qt::DisplayRole,temp);
                       text->setData(0,Qt::UserRole,iPaths[page]);
                       break;

                    }

                }
                connect(this,&QTreeWidget::itemClicked,this,&TreeWidgetSearchUniqueEntrys::textHighlighting);
            }
            else{break;}
        }


    }
    }
}

void TreeWidgetSearchUniqueEntrys::numberQuestion(const QStringList &outText, const QStringList &iPaths, const QString &searchedText)
{
    if(!searchedText.isEmpty()){
    for (int page = 0;page<outText.size();page++) {

        for (int i = 0;i<outText[page].size();i++) {
            if(outText[page].indexOf(" "+searchedText,i) != -1)
            {
                i = outText[page].indexOf(" "+searchedText,i);
                QString str = outText[page][i + searchedText.size()+1];
                QString str1 = outText[page][i + searchedText.size()+1];
                if(outText[page][i + searchedText.size()+2] == " " || outText[page][i + searchedText.size()+2] == "\n" )
                {

                QString temp = outText[page];
                for (int j = i;j < outText[page].size(); j--)
                {
                    if(outText[page][j] == " " || outText[page][j] == "\n")
                    {
                        QTreeWidgetItem* text = new QTreeWidgetItem(this);
                        items.push_back(text);
                       text->setData(0,Qt::UserRole +1 ,j);
                       temp.remove(0,j+1);
                       temp.remove(temp.indexOf(" "),temp.size() - temp.indexOf(" "));
                       if(temp.indexOf("\n") != -1){
                           temp.remove(temp.indexOf("\n"),temp.size() - temp.indexOf("\n"));
                       }
                       text->setData(0,Qt::DisplayRole,temp);
                       text->setData(0,Qt::UserRole,iPaths[page]);
                       break;

                    }

                }
                connect(this,&QTreeWidget::itemClicked,this,&TreeWidgetSearchUniqueEntrys::textHighlighting);
                }
            }

            else{break;}
        }
        for (int i = 0;i<outText[page].size();i++) {
            if(outText[page].indexOf("\n"+searchedText,i) != -1)
            {
                i = outText[page].indexOf("\n"+searchedText,i);
                if(outText[page][i + searchedText.size()+2] == " " || outText[page][i + searchedText.size()+2] == "\n")
                {

                QString temp = outText[page];
                for (int j = i;j < outText[page].size(); j--)
                {
                    if(outText[page][j] == " " || outText[page][j] == "\n")
                    {
                        QTreeWidgetItem* text = new QTreeWidgetItem(this);
                        items.push_back(text);
                       text->setData(0,Qt::UserRole +1 ,j);
                       temp.remove(0,j+1);
                       temp.remove(temp.indexOf(" "),temp.size() - temp.indexOf(" "));
                       if(temp.indexOf("\n") != -1){
                           temp.remove(temp.indexOf("\n"),temp.size() - temp.indexOf("\n"));
                       }
                       text->setData(0,Qt::DisplayRole,temp);
                       text->setData(0,Qt::UserRole,iPaths[page]);
                       break;

                    }

                }
                connect(this,&QTreeWidget::itemClicked,this,&TreeWidgetSearchUniqueEntrys::textHighlighting);
                }
            }

            else{break;}
        }
    }
    }

}

void TreeWidgetSearchUniqueEntrys::questionNumber(const QStringList &outText, const QStringList &iPaths, const QString &searchedText)
{
    if(!searchedText.isEmpty()){
    for (int page = 0;page<outText.size();page++) {

        for (int i = 0;i<outText[page].size();i++) {
            if(outText[page].indexOf(searchedText+" ",i) != -1)
            {
                i = outText[page].indexOf(searchedText+" ",i);
                if(outText[page][i - 2] == " " || outText[page][i - 2] == "\n" )
                {

                QString temp = outText[page];
                for (int j = i;j < outText[page].size(); j--)
                {
                    if(outText[page][j] == " " || outText[page][j] == "\n")
                    {
                        QTreeWidgetItem* text = new QTreeWidgetItem(this);
                        items.push_back(text);
                       text->setData(0,Qt::UserRole +1 ,j);
                       temp.remove(0,j+1);
                       temp.remove(temp.indexOf(" "),temp.size() - temp.indexOf(" "));
                       if(temp.indexOf("\n") != -1){
                           temp.remove(temp.indexOf("\n"),temp.size() - temp.indexOf("\n"));
                       }
                       text->setData(0,Qt::DisplayRole,temp);
                       text->setData(0,Qt::UserRole,iPaths[page]);
                       break;

                    }

                }
                connect(this,&QTreeWidget::itemClicked,this,&TreeWidgetSearchUniqueEntrys::textHighlighting);
                }
            }

            else{break;}
        }
        for (int i = 0;i<outText[page].size();i++) {
            if(outText[page].indexOf(searchedText+"\n",i) != -1)
            {
                i = outText[page].indexOf(searchedText+"\n",i);
                if(outText[page][i - 2] == "\n" || outText[page][i - 2] == " ")
                {

                QString temp = outText[page];
                for (int j = i;j < outText[page].size(); j--)
                {
                    if(outText[page][j] == " " || outText[page][j] == "\n")
                    {
                        QTreeWidgetItem* text = new QTreeWidgetItem(this);
                        items.push_back(text);
                       text->setData(0,Qt::UserRole +1 ,j);
                       temp.remove(0,j+1);
                       temp.remove(temp.indexOf(" "),temp.size() - temp.indexOf(" "));
                       if(temp.indexOf("\n") != -1){
                           temp.remove(temp.indexOf("\n"),temp.size() - temp.indexOf("\n"));
                       }
                       text->setData(0,Qt::DisplayRole,temp);
                       text->setData(0,Qt::UserRole,iPaths[page]);
                       break;

                    }

                }
                connect(this,&QTreeWidget::itemClicked,this,&TreeWidgetSearchUniqueEntrys::textHighlighting);
                }
            }

            else{break;}
        }
    }
    }

}

void TreeWidgetSearchUniqueEntrys::questionNumberQuestion(const QStringList &outText, const QStringList &iPaths, const QString &searchedText)
{
    if(!searchedText.isEmpty()){
    for (int page = 0;page<outText.size();page++) {

        for (int i = 0;i<outText[page].size();i++) {
            if(outText[page].indexOf(searchedText,i) != -1)
            {
                i = outText[page].indexOf(searchedText,i);
                if((outText[page][i - 2] == "\n" && outText[page][i + searchedText.size()+1] == "\n")
                        || (outText[page][i - 2] == " " && outText[page][i + searchedText.size()+1] == " ")
                        || (outText[page][i - 2] == " " && outText[page][i + searchedText.size()+1] == "\n")
                        || (outText[page][i - 2] == "\n" && outText[page][i + searchedText.size()+1] == " "))
                {

                QString temp = outText[page];
                for (int j = i;j < outText[page].size(); j--)
                {
                    if(outText[page][j] == " " || outText[page][j] == "\n")
                    {
                        QTreeWidgetItem* text = new QTreeWidgetItem(this);
                        items.push_back(text);
                       text->setData(0,Qt::UserRole +1 ,j);
                       temp.remove(0,j+1);
                       temp.remove(temp.indexOf(" "),temp.size() - temp.indexOf(" "));
                       if(temp.indexOf("\n") != -1){
                           temp.remove(temp.indexOf("\n"),temp.size() - temp.indexOf("\n"));
                       }
                       text->setData(0,Qt::DisplayRole,temp);
                       text->setData(0,Qt::UserRole,iPaths[page]);
                       break;

                    }

                }
                connect(this,&QTreeWidget::itemClicked,this,&TreeWidgetSearchUniqueEntrys::textHighlighting);
                }
            }

            else{break;}
        }

    }

    }
}

void TreeWidgetSearchUniqueEntrys::questionNumberPoint(const QStringList &outText, const QStringList &iPaths, const QString &searchedText)
{

    if(!searchedText.isEmpty()){
    for (int page = 0;page<outText.size();page++) {

        for (int i = 0;i<outText[page].size();i++) {
            if(outText[page].indexOf(searchedText,i) != -1)
            {
                i = outText[page].indexOf(searchedText,i);
                if(outText[page][i - 2] == " " || outText[page][i - 2] == "\n")
                {

                QString temp = outText[page];
                for (int j = i;j < outText[page].size(); j--)
                {
                    if(outText[page][j] == " " || outText[page][j] == "\n")
                    {
                        QTreeWidgetItem* text = new QTreeWidgetItem(this);
                        items.push_back(text);
                       text->setData(0,Qt::UserRole +1 ,j);
                       temp.remove(0,j+1);
                       temp.remove(temp.indexOf(" "),temp.size() - temp.indexOf(" "));
                       if(temp.indexOf("\n") != -1){
                           temp.remove(temp.indexOf("\n"),temp.size() - temp.indexOf("\n"));
                       }
                       text->setData(0,Qt::DisplayRole,temp);
                       text->setData(0,Qt::UserRole,iPaths[page]);
                       break;

                    }

                }
                connect(this,&QTreeWidget::itemClicked,this,&TreeWidgetSearchUniqueEntrys::textHighlighting);
                }
            }

            else{break;}
        }
    }

    }
}

void TreeWidgetSearchUniqueEntrys::pointNumberQuestion(const QStringList &outText, const QStringList &iPaths, const QString &searchedText)
{
    if(!searchedText.isEmpty()){
    for (int page = 0;page<outText.size();page++) {

        for (int i = 0;i<outText[page].size();i++) {
            if(outText[page].indexOf(searchedText,i) != -1)
            {
                i = outText[page].indexOf(searchedText,i);
                if(outText[page][i + searchedText.size()+1] == " " || outText[page][i + searchedText.size()+1] == "\n")
                {

                QString temp = outText[page];
                for (int j = i;j < outText[page].size(); j--)
                {
                    if(outText[page][j] == " " || outText[page][j] == "\n")
                    {
                        QTreeWidgetItem* text = new QTreeWidgetItem(this);
                        items.push_back(text);
                       text->setData(0,Qt::UserRole +1 ,j);
                       temp.remove(0,j+1);
                       temp.remove(temp.indexOf(" "),temp.size() - temp.indexOf(" "));
                       if(temp.indexOf("\n") != -1){
                           temp.remove(temp.indexOf("\n"),temp.size() - temp.indexOf("\n"));
                       }
                       text->setData(0,Qt::DisplayRole,temp);
                       text->setData(0,Qt::UserRole,iPaths[page]);
                       break;

                    }

                }
                connect(this,&QTreeWidget::itemClicked,this,&TreeWidgetSearchUniqueEntrys::textHighlighting);
                }
            }

            else{break;}
        }
    }
    }
}

void TreeWidgetSearchUniqueEntrys::pointNumberPoint(const QStringList &outText, const QStringList &iPaths, const QString &searchedText)
{

    if(!searchedText.isEmpty()){
    for (int page = 0;page<outText.size();page++) {

        for (int i = 0;i<outText[page].size();i++) {
            if(outText[page].indexOf(searchedText,i) != -1)
            {
                i = outText[page].indexOf(searchedText,i);

                QString temp = outText[page];
                for (int j = i;j < outText[page].size() && j >= 0; j--)
                {

                    if(outText[page][j] == " " || outText[page][j] == "\n")
                    {
                        QTreeWidgetItem* text = new QTreeWidgetItem(this);
                        items.push_back(text);
                       text->setData(0,Qt::UserRole +1 ,j);
                       temp.remove(0,j+1);
                       temp.remove(temp.indexOf(" "),temp.size() - temp.indexOf(" "));
                       if(temp.indexOf("\n") != -1){
                           temp.remove(temp.indexOf("\n"),temp.size() - temp.indexOf("\n"));
                       }
                       text->setData(0,Qt::DisplayRole,temp);
                       text->setData(0,Qt::UserRole,iPaths[page]);
                       break;

                    }

                }
                connect(this,&QTreeWidget::itemClicked,this,&TreeWidgetSearchUniqueEntrys::textHighlighting);

            }

            else{break;}
        }
    }
    }

}

QString TreeWidgetSearchUniqueEntrys::getPath() const
{
    return m_path;
}

void TreeWidgetSearchUniqueEntrys::setPath(const QString &path)
{
    m_path = path;
}

void TreeWidgetSearchUniqueEntrys::textHighlighting(QTreeWidgetItem *item, int page)
{
    if(item->data(0,Qt::DisplayRole).toString() != ""){
    QString _text = item->data(0,Qt::DisplayRole).toString();
    QString list = item->data(0,Qt::UserRole).toString();
    wiz->centerOnItem(list);
    wiz->drawRect(_text,list);
    QString str = wiz->recTextFromPath(list);
    int index = item->data(0,Qt::UserRole+1).toInt();
    int begin = index+1;
    int end = begin + _text.size() +1;
    QTextCharFormat fmt;
    fmt.setFontPointSize(34);
    QTextCursor cursor = wiz->getLists()[wiz->indexOfRecText(list)]->textCursor();//lists[list-1]->textCursor();
    cursor.setPosition(begin,QTextCursor::MoveAnchor);
    cursor.setPosition(end,QTextCursor::KeepAnchor);
    cursor.setCharFormat(fmt);
    wiz->getLists()[wiz->indexOfRecText(list)]->setTextCursor(cursor);
    }
}
