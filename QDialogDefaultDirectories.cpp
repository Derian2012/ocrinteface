#include "QDialogDefaultDirectories.h"

QDialogDefaultDirectories::QDialogDefaultDirectories()
{
    this->resize(600,200);

    saveButton->setFixedSize(100,30);
    saveButton->setText("Save");
    setDirectoriesButton->setFixedSize(80,30);
    setDirectoriesButton->setText("Add");
    fileDirectoriesLine->setMinimumSize(300,30);
    fileDirectoriesLine->setText("Push Add to select file directory");
    fileDirectoriesLine->setStyleSheet("color: gray");

    QBoxLayout* mainLayout = new QBoxLayout(QBoxLayout::BottomToTop);
    QBoxLayout* buttonLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    QBoxLayout* lineAndAddLayout = new QBoxLayout(QBoxLayout::LeftToRight);


    buttonLayout->addWidget(saveButton,1);
    buttonLayout->setSpacing(400);

    lineAndAddLayout->addWidget(fileDirectoriesLine,0);
    lineAndAddLayout->addWidget(setDirectoriesButton,1);
    lineAndAddLayout->setSpacing(40);

    mainLayout->addLayout(buttonLayout,0);
    mainLayout->addLayout(lineAndAddLayout,1);
    mainLayout->setSpacing(50);
    this->setLayout(mainLayout);

    connect(setDirectoriesButton,&QPushButton::clicked,this,&QDialogDefaultDirectories::addFileDirectories);
    connect(saveButton,&QPushButton::clicked,this,&QDialogDefaultDirectories::close);
    connect(fileDirectoriesLine,&QLineEdit::textChanged,this,&QDialogDefaultDirectories::createNewDir);

}

void QDialogDefaultDirectories::addFileDirectories()
{
    setDirectory(QFileDialog::getExistingDirectory());
    fileDirectoriesLine->setText(getDirectory());
    fileDirectoriesLine->setStyleSheet("color: black");

}

void QDialogDefaultDirectories::createNewDir(const QString &dirPath)
{
    QDir* directory = new QDir();
    directory->setPath(dirPath);
    emit sendDir(directory);
}

QString QDialogDefaultDirectories::getDirectory() const
{
    return directory;
}

void QDialogDefaultDirectories::setDirectory(const QString &value)
{
    directory = value;
}




