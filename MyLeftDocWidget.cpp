#include "MyLeftDocWidget.h"

MyLeftDocWidget::MyLeftDocWidget(QWidget* widget): QDockWidget (widget)
{

}
/*
bool MyLeftDocWidget::resizeEventFilter(QResizeEvent* event)
{
        emit docSizeChange(event->size());
    return QWidget::eventFilter(this,event);
}
*/
bool MyLeftDocWidget::eventFilter(QObject *obj, QEvent *event) {
  if (event->type() == QEvent::Resize) {
      QResizeEvent *resizeEvent = static_cast<QResizeEvent*>(event);
    emit docSizeChange(resizeEvent->size());
  }
  return QWidget::eventFilter(obj, event);
}
