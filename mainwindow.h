#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "mygraphicview.h"
#include <QIcon>
#include <QAction>
#include <QMainWindow>
#include <QWidget>
#include <QDockWidget>
#include <QString>
#include <QTextEdit>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QTreeWidget>
#include <QToolBar>
#include <QFileDialog>
#include <QTreeWidget>
#include <QProgressBar>
#include <QLineEdit>
#include <QFileSystemWatcher>
#include "OCR.h"
#include "TextWizard.h"
#include "RightDocWidget.h"
#include "docTree.h"
#include "mygraphicview.h"
#include "QDialogDefaultDirectories.h"
#include "MyLeftDocWidget.h"
#include "TreeWidgetSearchUniqueEntrys.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    TextWizard *currentWiz() const;
    void setCurrentWiz(TextWizard *currentWiz);
    RDocWidget *rightWidget() const;
    void setRightWidget(RDocWidget *rightWidget);  
    QStringList* selectedFiles() const;
    void setSelectedFiles(const QStringList& selectedFiles);
    void addToallFiles(const QStringList& str);
    QStringList* allFiles() const;
    DocTree *leftTreeView() const;
    void setLeftTreeView(DocTree *leftTreeView);
    QDockWidget *leftDocWidget() const;
    void setLeftDocWidget(QDockWidget *leftDocWidget);
    QStringList createNewDoc(QDir dir , int level = 0);
    void changeDocOnDefault();
    void createPdfDirectory(const QString& pdfFilePath, const QString& directoryOfFile);
    void createSinglePdfDirectory(const QString& pdfFilePath, const QString& directoryOfFile);





public slots:
    void addList();
    void addNum();
    void recognizeSelectedDir();
    void recognizeSelectedSingle();
    void changeDoc(QModelIndex index);
    void changeSingleDoc(QModelIndex index);
    void createDocsFromDir(QDir* directory);
    void recognizeAllDir();
    void recognizeAllSingle();
    void refreshDirectory(const QString path);
    void swichViewModel();
    void sendTextToWidget(const QString& searchedText);
    void changeSearchMode();



private:


    QFileSystemWatcher* watcher = new QFileSystemWatcher();
    QDialogDefaultDirectories* dialog = new QDialogDefaultDirectories();

    QList<TextWizard*> wizrds;   
    QList<TextWizard*> directoryWizards;

    QList<RDocWidget*> rightWidgets;
    QProgressBar* prBar = new QProgressBar();
    QThread* progressBatThread = new QThread(this);
    QLineEdit* searchLine = new QLineEdit();

    MyGraphicsView* mainView =new MyGraphicsView(this);
    TextWizard* m_currentWiz = new TextWizard(this, mainView,prBar);
    RDocWidget* m_rightWidget = new RDocWidget(m_currentWiz);
    DocTree* m_leftTreeViewForDir = new DocTree(this,m_currentWiz);
    DocTree* m_leftTreeViewForSingleDoc = new DocTree(this,m_currentWiz);
    QDockWidget* rigtDoc= new QDockWidget (m_rightWidget);

    QList<QStringList*> m_allFiles;    
    QStringList* m_selectedFiles=  new QStringList();
    //QStringList* m_currentFiles;
    QList<QStringList*> m_allDirFiles;
    QStringList* m_currentDirFiles;

    QDockWidget* m_leftDocWidget= new QDockWidget(this);
    MyLeftDocWidget* m_leftDocWidgetForDir = new MyLeftDocWidget(this);
    MyLeftDocWidget* m_leftDocWidgetForSingleDoc = new MyLeftDocWidget(this);

    TreeWidgetSearchUniqueEntrys* m_rightSearchWidget = new TreeWidgetSearchUniqueEntrys(this,m_currentWiz);
    QList<TreeWidgetSearchUniqueEntrys*> rightSearchWidgets;


};

#endif // MAINWINDOW_H
