#include "mygraphicview.h"
#include <QObject>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QWheelEvent>
#include <QMainWindow>


MyGraphicsView::MyGraphicsView(QGraphicsScene *scene, QWidget *parent):
    QGraphicsView(scene,parent)
{
    this->scale(0.1,0.1);
}

MyGraphicsView::MyGraphicsView(QWidget *parent):
    QGraphicsView(parent)
{
 this->scale(0.5,0.5);
}

void MyGraphicsView::wheelEvent(QWheelEvent *event)
{

    if (event->modifiers() & Qt::ControlModifier)
    {

        if (event->delta() > 0)
        {
            scale(1.015, 1.015);
        }
        else
        {
            scale(0.985, 0.985);
        }
    }

    else
    {
        QGraphicsView::wheelEvent(event);

    }
}
