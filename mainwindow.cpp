#include "mainwindow.h"
#include "TextWizard.h"

#include <QDebug>
#include <QErrorMessage>
#include <QErrorMessage>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{

    mainView->scale(0.5,0.5);
    prBar->setRange(0,100);
    QDir* dir = new QDir("/home/aleksandr/Изображения/images (копия)/innerImages/innerimages0");
    createDocsFromDir(dir);
    watcher->addPath(dir->path());

    QToolBar* bar = new QToolBar();
    QDockWidget* bottomDocWidget = new QDockWidget(this);
    QDockWidget* searchDocWidget = new QDockWidget(this);

    bar->addAction("num",this,SLOT(addNum()));
    bar->addAction("Swich",this,&MainWindow::swichViewModel);
    bar->addAction("Change search mode",this,&MainWindow::changeSearchMode);


    addToolBar(Qt::TopToolBarArea,bar);
    addDockWidget(Qt::RightDockWidgetArea,rigtDoc);
    addDockWidget(Qt::RightDockWidgetArea,searchDocWidget);
    addDockWidget(Qt::BottomDockWidgetArea,bottomDocWidget);
    addDockWidget(Qt::LeftDockWidgetArea,m_leftDocWidgetForDir);
    addDockWidget(Qt::LeftDockWidgetArea,m_leftDocWidgetForSingleDoc);


    progressBatThread->start();

    searchDocWidget->setWidget(searchLine);
    bottomDocWidget->setWidget(prBar);
    //rigtDoc->setWidget(rightDocWin);
    rigtDoc->setWidget(m_rightSearchWidget);

    QMainWindow* winForDir = new QMainWindow();
    QToolBar* barForDir = new QToolBar();
    barForDir->addAction("Recognize",this,&MainWindow::recognizeSelectedDir);
    barForDir->addAction("RecognizeAll",this,&MainWindow::recognizeAllDir);
    barForDir->addAction("Default Directories",dialog,&QDialogDefaultDirectories::open);
    winForDir->addToolBar(Qt::TopToolBarArea,barForDir);
    winForDir->setCentralWidget(m_leftTreeViewForDir);
    m_leftDocWidgetForDir->setWidget(winForDir);

    QMainWindow* winForSingleDoc = new QMainWindow();
    QToolBar* barForSingleDoc  = new QToolBar();
    barForSingleDoc ->addAction("Recognize",this,&MainWindow::recognizeSelectedSingle);
    barForSingleDoc ->addAction("RecognizeAll",this,&MainWindow::recognizeAllSingle);
    barForSingleDoc->addAction("AddDoc",this,&MainWindow::addList);
    winForSingleDoc->addToolBar(Qt::TopToolBarArea,barForSingleDoc);
    winForSingleDoc->setCentralWidget(m_leftTreeViewForSingleDoc);
    m_leftDocWidgetForSingleDoc->setWidget(winForSingleDoc);



    m_leftDocWidgetForDir->setMinimumWidth(250);    
    m_leftTreeViewForSingleDoc->newDocTree(m_currentWiz, "Docs");

    m_leftDocWidgetForSingleDoc->setMinimumWidth(250);

    m_leftDocWidgetForDir->installEventFilter(m_leftDocWidgetForDir);

    connect(m_leftTreeViewForDir,&DocTree::clicked,this, &MainWindow::changeDoc);
    connect(this,&MainWindow::destroyed,progressBatThread,&QThread::quit);
    connect(dialog,&QDialogDefaultDirectories::sendDir,this,&MainWindow::createDocsFromDir);
    connect(m_leftDocWidgetForDir,&MyLeftDocWidget::docSizeChange, m_leftTreeViewForDir, &DocTree::changeText);
    connect(watcher,&QFileSystemWatcher::directoryChanged,this,&MainWindow::refreshDirectory);
    connect(searchLine,&QLineEdit::textChanged,this,&MainWindow::sendTextToWidget);
    connect(m_leftTreeViewForSingleDoc,&DocTree::clicked,this, &MainWindow::changeSingleDoc);



}

MainWindow::~MainWindow()
{

}

void MainWindow::addList()
{
    QStringList files = QFileDialog::getOpenFileNames(this,"Open File");

    int sepCount = files[0].count(QDir::separator());
    QString path = files[0].section(QDir::separator(),0,sepCount-1);
    QString name = files[0].section(QDir::separator(),sepCount,sepCount);
    path += ".Single";

    if(m_leftTreeViewForSingleDoc->parentItemIsExist(path))
    {
for (int j =0 ;j <files.size(); j++) {

        for (int i = 0; i < directoryWizards.size();i++) {
            if(directoryWizards[i]->getPath() == path)
            {
                m_currentWiz = directoryWizards[i];
            }

        }
        for (int i = 0; i < rightWidgets.size();i++) {
            if(rightWidgets[i]->getPath() == path)
            {
                m_rightWidget = rightWidgets[i];
            }


        }
        //m_currentDirFiles = m_allDirFiles[i];
        rigtDoc->setWidget(m_rightSearchWidget);
        mainView->setScene(m_currentWiz);
        m_leftTreeViewForSingleDoc->changeParentItem(path);
        m_leftTreeViewForSingleDoc->changeWiz(m_currentWiz);
        this->setCentralWidget(mainView);
        searchLine->clear();



        if(files[j].contains(".pdf"))
        {
            createSinglePdfDirectory(files[j],path);
        }
        else {
        currentWiz()->addImage(QStringList() << files[j]);
        m_leftTreeViewForSingleDoc->addFile(QStringList() << files[j],QStringList() << files[j]);
}
}
    }

    else {
        m_currentWiz = new TextWizard(this,mainView,prBar);
        m_currentWiz->setPath(path);
        directoryWizards.push_back(m_currentWiz);
        m_rightWidget = new RDocWidget(m_currentWiz);
        rightWidgets.push_back(m_rightWidget);
        m_leftTreeViewForSingleDoc->changeParentItem("Docs");
        m_leftTreeViewForSingleDoc->createParentItem(currentWiz(),path,"Docs");

        for (int j =0 ;j <files.size(); j++) {

        if(files[j].contains(".pdf"))
        {
            createSinglePdfDirectory(files[j],path);
            for (int i = 0; i < directoryWizards.size();i++) {
                if(directoryWizards[i]->getPath() == path)
                {
                    m_currentWiz = directoryWizards[i];
                }

            }
            for (int i = 0; i < rightWidgets.size();i++) {
                if(rightWidgets[i]->getPath() == path)
                {
                    m_rightWidget = rightWidgets[i];
                }


            }
            //m_currentDirFiles = m_allDirFiles[i];
            rigtDoc->setWidget(m_rightSearchWidget);
            mainView->setScene(m_currentWiz);
            m_leftTreeViewForSingleDoc->changeParentItem(path);
            m_leftTreeViewForSingleDoc->changeWiz(m_currentWiz);
            this->setCentralWidget(mainView);
            searchLine->clear();
        }
        else {
        currentWiz()->addImage(QStringList() << files[j]);
        m_leftTreeViewForSingleDoc->addFile(QStringList() << files[j],QStringList() << files[j]);
}
}
        connect(searchLine,&QLineEdit::textChanged,m_rightWidget,&RDocWidget::numbersFilter);
    }

}

void MainWindow::addNum()
{   
    rightWidget()->addNumbers(currentWiz()->recText(),currentWiz()->getRecognizedFiles());

}

void MainWindow::recognizeSelectedDir()
{

    currentWiz()->setInPrigress(true);
    currentWiz()->createList(m_leftTreeViewForDir->chekedItems(),m_leftTreeViewForDir->getChekedItemsIndexes());
}

void MainWindow::recognizeSelectedSingle()
{
    currentWiz()->setInPrigress(true);
    currentWiz()->createList(m_leftTreeViewForSingleDoc->chekedItems(),m_leftTreeViewForSingleDoc->getChekedItemsIndexes());
}


QStringList MainWindow::createNewDoc(QDir directory, int level)
{

    QStringList files = directory.entryList(QStringList() <<"*.jpg" << "*.png",QDir::Filter::Files);
    QStringList pdfFiles = directory.entryList(QStringList() << "*.pdf",QDir::Filter::Files);
    QStringList dirs = directory.entryList((QDir::Filter::NoDotAndDotDot | QDir::Dirs));


    QStringList names;
    for (int i = 0;i < files.size();i++) {
        names.push_back(files[i]);
        files[i]= directory.absoluteFilePath(files[i]);
    }

//    m_currentDirFiles = new QStringList(files);
//    m_allDirFiles.push_back(m_currentDirFiles);
    m_currentWiz = new TextWizard(this,mainView,prBar);
    m_currentWiz->setPath(directory.path());
    directoryWizards.push_back(m_currentWiz);
    m_rightWidget = new RDocWidget(m_currentWiz);
    rightWidgets.push_back(m_rightWidget);
    currentWiz()->addImage(files);
    m_leftTreeViewForDir->addFile(files,names);
    connect(searchLine,&QLineEdit::textChanged,m_rightWidget,&RDocWidget::numbersFilter);


    if(!pdfFiles.empty())
    {
        for (int i =0 ; i <pdfFiles.size();i++) {
            pdfFiles[i] = directory.absoluteFilePath(pdfFiles[i]);
            createPdfDirectory(pdfFiles[i],directory.path());
            //create pdf
        }
    }


    if (!dirs.empty())
    {

        int dirSize = dirs.size();
        QStringList tempDir = dirs;
        for (int i = 0; i < dirSize;i++)
        {

            QDir dir(directory.absoluteFilePath(tempDir[i]));
            watcher->addPath(dir.path());
            m_leftTreeViewForDir->createParentItem(m_currentWiz,dir.path(), directory.path());
            dirs = createNewDoc(dir,(level+1));
        }

    }

    return dirs;
}

void MainWindow::createPdfDirectory(const QString &pdfFilePath, const QString &directoryOfFile)
{
    m_leftTreeViewForDir->createParentItem(m_currentWiz,pdfFilePath, directoryOfFile);
    m_currentWiz = new TextWizard(this,mainView,prBar);
    m_currentWiz->setPath(pdfFilePath);
    directoryWizards.push_back(m_currentWiz);
    m_rightWidget = new RDocWidget(m_currentWiz);
    rightWidgets.push_back(m_rightWidget);
    currentWiz()->addImage(QStringList()<<pdfFilePath);
    Poppler::Document* document = Poppler::Document::load(pdfFilePath);
    QStringList files;
    QStringList names;
    for (int i = 0;i<document->numPages(); i++) {
        files.push_back(pdfFilePath+QDir::separator() + QString::number(i));
        names.push_back("page "+QString::number(i));
    }
    m_leftTreeViewForDir->addFile(files,names);
    connect(searchLine,&QLineEdit::textChanged,m_rightWidget,&RDocWidget::numbersFilter);
}

void MainWindow::createSinglePdfDirectory(const QString &pdfFilePath, const QString &directoryOfFile)
{
    m_leftTreeViewForSingleDoc->createParentItem(m_currentWiz,pdfFilePath, directoryOfFile);
    m_currentWiz = new TextWizard(this,mainView,prBar);
    m_currentWiz->setPath(pdfFilePath);
    directoryWizards.push_back(m_currentWiz);
    m_rightWidget = new RDocWidget(m_currentWiz);
    rightWidgets.push_back(m_rightWidget);
    currentWiz()->addImage(QStringList()<<pdfFilePath);
    Poppler::Document* document = Poppler::Document::load(pdfFilePath);
    QStringList files;
    QStringList names;
    for (int i = 0;i<document->numPages(); i++) {
        files.push_back(pdfFilePath+QDir::separator() + QString::number(i));
        names.push_back("page "+QString::number(i));
    }
    m_leftTreeViewForSingleDoc->addFile(files,names);
    connect(searchLine,&QLineEdit::textChanged,m_rightWidget,&RDocWidget::numbersFilter);

}

void MainWindow::changeDocOnDefault()
{

    m_currentWiz = directoryWizards[0];
    m_rightWidget = rightWidgets[0];
    //m_rightSearchWidget = rightSearchWidgets[0];
   // rightDocWin->setCentralWidget(m_rightSearchWidget);
    //m_currentDirFiles = m_allDirFiles[i];
    //rigtDoc->setWidget(m_rightWidget);
    mainView->setScene(m_currentWiz);
    m_leftTreeViewForDir->changeParentItem(0);
    m_leftTreeViewForDir->changeWiz(m_currentWiz);
    this->setCentralWidget(mainView);
    searchLine->clear();

}

void MainWindow::recognizeAllDir()
{
    for(int i =0; i < m_leftTreeViewForDir->getParentItems().size();i++)
    {

        m_currentWiz = directoryWizards[i];
        m_rightWidget = rightWidgets[i];
        rigtDoc->setWidget(m_rightWidget);
        mainView->setScene(m_currentWiz);
        m_leftTreeViewForDir->changeParentItem(i);
        m_leftTreeViewForDir->changeWiz(m_currentWiz);
        searchLine->clear();
        m_currentWiz->setInPrigress(true);
        currentWiz()->createList(m_leftTreeViewForDir->chekedItems(),m_leftTreeViewForDir->getChekedItemsIndexes());

    }
}

void MainWindow::recognizeAllSingle()
{
    for(int i =0; i < m_leftTreeViewForSingleDoc->getParentItems().size();i++)
    {
        m_currentDirFiles = m_allDirFiles[i];
        m_currentWiz = directoryWizards[i];
        m_rightWidget = rightWidgets[i];
        rigtDoc->setWidget(m_rightWidget);
        mainView->setScene(m_currentWiz);
        m_leftTreeViewForSingleDoc->changeParentItem(i);
        m_leftTreeViewForSingleDoc->changeWiz(m_currentWiz);
        searchLine->clear();
        m_currentWiz->setInPrigress(true);
        currentWiz()->createList(m_leftTreeViewForSingleDoc->chekedItems(),m_leftTreeViewForSingleDoc->getChekedItemsIndexes());

    }

}

void MainWindow::refreshDirectory(const QString path)
{
    QDir dir(path);
    QStringList filesInDirectory = dir.entryList(QStringList() <<"*.jpg" << "*.png",QDir::Filter::Files);
    QStringList foldersInDirectory = dir.entryList((QDir::Filter::NoDotAndDotDot | QDir::Dirs));

    if (!filesInDirectory.empty()){
        for (int i = 0;i <directoryWizards.size();i++) {
            if (directoryWizards[i]->getPath() == path)
            {
                m_currentWiz = directoryWizards[i];
            }
        }
        m_leftTreeViewForDir->changeParentItem(path);
        m_leftTreeViewForDir->changeWiz(m_currentWiz);

        for (int i = 0;i < filesInDirectory .size();i++) {
            filesInDirectory [i]= dir.absoluteFilePath(filesInDirectory [i]);
        }

        if (filesInDirectory.size() == m_currentWiz->getFiles().size())
        {

            for(int i = 0; i<filesInDirectory.size();i++){
                if (m_currentWiz->getFiles().indexOf(filesInDirectory[i]) == -1){
                    for (int j =0; j<m_currentWiz->getFiles().size();j++)
                    {
                        if(filesInDirectory.indexOf(m_currentWiz->getFiles()[j]) == -1)
                        {
                            m_leftTreeViewForDir->imageChanged(m_currentWiz->getFiles()[j],filesInDirectory[i]);
                            currentWiz()->changeFiles(j,filesInDirectory[i]);
                        }
                    }

                }
            }

        } else if (filesInDirectory.size() > m_currentWiz->getFiles().size())
        {

            for(int i = 0; i<filesInDirectory.size();i++){
                if (m_currentWiz->getFiles().indexOf(filesInDirectory[i]) == -1){
                    QString name = filesInDirectory[i];
                    for (int j =filesInDirectory[i].size()-1;j > 0  ; j--) {
                        if(filesInDirectory[i][j] == QDir::separator())
                        {
                            name.remove(0,j+1);
                            break;
                        }
                    }
                    m_leftTreeViewForDir->addFile(QStringList()<< filesInDirectory[i],QStringList()<<name);
                    //currentWiz()->addToFiles(filesInDirectory[i]);
                    currentWiz()->addImage(QStringList()<< filesInDirectory[i]);
                    //add this file in wiz and tree
                }
            }
        }
        else {

            for(int i = m_currentWiz->getFiles().size()-1; i >= 0;i--){
                if (filesInDirectory.indexOf(m_currentWiz->getFiles()[i]) == -1){
                    m_currentWiz->removeImage(m_currentWiz->getFiles()[i]);
                    m_currentWiz->removeWordsData(m_currentWiz->getFiles()[i]);
                    m_leftTreeViewForDir->removeFile(m_currentWiz->getFiles()[i]);
                    m_currentWiz->removeFile(m_currentWiz->getFiles()[i]);
                    // remove this file from wiz and tree
                }

            }

        }
    }

    if (!foldersInDirectory.empty()){

        m_leftTreeViewForDir->changeParentItem(path);

        for (int i = 0;i < foldersInDirectory.size();i++) {
            foldersInDirectory [i]= dir.absoluteFilePath(foldersInDirectory [i]);
        }
        if(m_leftTreeViewForDir->getChildren().size() > foldersInDirectory.size()){

            for (int i = 0;i < m_leftTreeViewForDir->getChildren().size();i++) {
                if (foldersInDirectory.indexOf(m_leftTreeViewForDir->getChildren()[i]) == -1){
                    watcher->removePath(m_leftTreeViewForDir->getChildren()[i]);
                    m_leftTreeViewForDir->removeParentItem(m_leftTreeViewForDir->getChildren()[i]);
                    for (int i = 0; i < rightWidgets.size();i++) {
                        if (rightWidgets[i]->getPath() == path)
                        {
                            rightWidgets.removeAt(i);
                        }
                    }

                                        for (int j = 0;j <directoryWizards.size();j++) {
                                            if (directoryWizards[j]->getPath() == path)
                                            {
                                                m_currentWiz = directoryWizards[j];
                                                delete m_currentWiz;
                                                directoryWizards.removeAt(j);                                               
                                            }

                                        }


                }

            }
            changeDocOnDefault();
        }else {
            for (int i = 0;i < foldersInDirectory.size();i++) {
                QStringList temp  = m_leftTreeViewForDir->getChildren();
                if ((m_leftTreeViewForDir->getChildren().indexOf(foldersInDirectory[i]) == -1)){
                    watcher->addPath(foldersInDirectory[i]);
                    m_currentWiz = new TextWizard(this,mainView,prBar);
                    m_currentWiz->setPath(foldersInDirectory[i]);
                    //directoryWizards.push_back(m_currentWiz);
                    m_currentWiz->deleteLater();
                    m_leftTreeViewForDir->createParentItem(m_currentWiz,foldersInDirectory[i],path);
                    createNewDoc(QDir(foldersInDirectory[i]));
                    m_leftTreeViewForDir->changeParentItem(path);
                }
            }


        }

    }

}

void MainWindow::swichViewModel()
{
    for (int i =0; i < directoryWizards.size(); i++) {
        directoryWizards[i]->changeViewModel();
    }

}

void MainWindow::sendTextToWidget( const QString &searchedText)
{
    QString temp = searchedText;
    m_rightSearchWidget->searchEntry(currentWiz()->recText(),currentWiz()->getRecognizedFiles(),temp,m_currentWiz);
}

void MainWindow::changeSearchMode()
{

   if (rigtDoc->widget() == m_rightSearchWidget)
   {
       rigtDoc->setWidget(m_rightWidget);
   }
   else {
       rigtDoc->setWidget(m_rightSearchWidget);
   }

}

void MainWindow::changeDoc(QModelIndex index)
{
    if( m_leftTreeViewForDir->getCurrentModel()->itemFromIndex(index)->data(Qt::UserRole+1).toBool() == true)
    {
        QString path = m_leftTreeViewForDir->getCurrentModel()->itemFromIndex(index)->data(Qt::UserRole+2).toString();
        for (int i = 0; i < directoryWizards.size();i++) {
            if(directoryWizards[i]->getPath() == path)
            {
                m_currentWiz = directoryWizards[i];
            }

        }
                for (int i = 0; i < rightWidgets.size();i++) {
                    if(rightWidgets[i]->getPath() == path)
                    {
                        m_rightWidget = rightWidgets[i];
                    }


        }
        //m_currentDirFiles = m_allDirFiles[i];
        rigtDoc->setWidget(m_rightSearchWidget);
        mainView->setScene(m_currentWiz);
        m_leftTreeViewForDir->changeParentItem(path);
        m_leftTreeViewForDir->changeWiz(m_currentWiz);
        this->setCentralWidget(mainView);
        searchLine->clear();
    }

}

void MainWindow::changeSingleDoc(QModelIndex index)
{
    if( m_leftTreeViewForSingleDoc->getCurrentModel()->itemFromIndex(index)->data(Qt::UserRole+1).toBool() == true)
    {
        QString path = m_leftTreeViewForSingleDoc->getCurrentModel()->itemFromIndex(index)->data(Qt::UserRole+2).toString();
        for (int i = 0; i < directoryWizards.size();i++) {
            if(directoryWizards[i]->getPath() == path)
            {
                m_currentWiz = directoryWizards[i];
            }

        }
                for (int i = 0; i < rightWidgets.size();i++) {
                    if(rightWidgets[i]->getPath() == path)
                    {
                        m_rightWidget = rightWidgets[i];
                    }


        }
        //m_currentDirFiles = m_allDirFiles[i];
        rigtDoc->setWidget(m_rightSearchWidget);
        mainView->setScene(m_currentWiz);
        m_leftTreeViewForSingleDoc->changeParentItem(path);
        m_leftTreeViewForSingleDoc->changeWiz(m_currentWiz);
        this->setCentralWidget(mainView);
        searchLine->clear();
    }

}

void MainWindow::createDocsFromDir(QDir *directory)
{
    m_leftTreeViewForDir->newDocTree(m_currentWiz, directory->path());
    createNewDoc(*directory);
    changeDocOnDefault();
}

QDockWidget *MainWindow::leftDocWidget() const
{
    return m_leftDocWidget;
}

void MainWindow::setLeftDocWidget(QDockWidget *leftDocWidget)
{
    m_leftDocWidget = leftDocWidget;
}

/*
QStringList* MainWindow::allFiles() const
{
    return m_currentFiles;;
}
*/

QStringList* MainWindow::selectedFiles() const
{
    return m_selectedFiles;
}

void MainWindow::setSelectedFiles(const QStringList& selectedFiles)
{
    *m_selectedFiles = selectedFiles;
}

/*
void MainWindow::addToallFiles(const QStringList& str)
{
    m_currentFiles->append(str);
}
*/

RDocWidget *MainWindow::rightWidget() const
{
    return m_rightWidget;
}

void MainWindow::setRightWidget(RDocWidget *rightWidget)
{
    m_rightWidget = rightWidget;
}

TextWizard *MainWindow::currentWiz() const
{
    return m_currentWiz;
}

void MainWindow::setCurrentWiz(TextWizard *currentWiz)
{
    m_currentWiz = currentWiz;
}



