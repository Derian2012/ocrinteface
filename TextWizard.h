#ifndef TEXTWIZARD_H
#define TEXTWIZARD_H
#include <QGraphicsScene>
#include <QMainWindow>
#include <QTextEdit>
#include <QGraphicsItem>
#include <QGraphicsProxyWidget>
#include <QGraphicsPixmapItem>
#include "mygraphicview.h"
#include "OCR.h"
#include <QPushButton>
#include <QThread>
#include <QProgressBar>


class TextWizard: public QGraphicsScene
{
    Q_OBJECT
public:

    struct WordData{QString level= "";
                    QString page_num= "";
                    QString block_num= "";
                    QString par_num = "";
                    QString line_num = "";
                    QString word_num = "";
                    QString left = "";
                    QString top = "";
                    QString width = "";
                    QString height = "";
                    QString conf = "";
                    QString text = "";
                    QString iPath = "";};

    TextWizard();
    TextWizard(QMainWindow* window , MyGraphicsView* mainView, QProgressBar* progressBar );
    ~TextWizard();
    void createList(const QStringList& iPath,QList<int> indexes);
    QStringList recText() const;
    QString recTextAtIndex(int index);
    void addRecText(const QStringList &recText);
    OCR *getCurrentOcr() const;
    void setCurrentOcr(OCR *value);
    MyGraphicsView *getView() const;
    void centerOnItem(int index);
    void centerOnItem(QString iPath);
    void addImage(const QStringList& iPath);
    QString recTextFromPath(const QString iPath);
    double scaleOfImageFromPath(const QString& iPath);
    int indexOfRecText(const QString iPath);
    void changeWiz(QMainWindow* wind);
    QList<QTextEdit *> getLists() const;
    void setLists(const QList<QTextEdit *> &value);
    void sendSignalPush(QGraphicsItem* item);
    QString getPath() const;
    void setPath(const QString &value);
    void addToFiles(const QString path);
    void removeFile(const QString path);

    QStringList getFiles() const;
    void changeFiles(int index,QString fileName);
    void removeImage(const QString fileName);
    void removeWordsData(const QString fileName);

    QStringList getRecognizedFiles() const;
    void removeRecognizedFile(int index);
    void setRecognizedFiles(const QStringList &value);
    void drawRect(QString text,QString iPath);
void changeViewModel();
bool setWordsData(QString TSVtext, QString iPath);


void setInPrigress(bool value);

public slots:
void showHide(QGraphicsItem* item);
    void addTextEditInWizard(const QStringList& iPaths);


 signals:
    void buttonPushed(QGraphicsItem*);
    void recognize(const QStringList& iPath);


private:
    bool inProgress = false;
    bool isItColum = false;
    QString path;
    QStringList files;
    QStringList recognizedFiles;
    MyGraphicsView* view;
    OCR* currentOcr = new OCR();
    QThread* thread;
    QStringList m_recText;
    QList <QTextEdit*> lists;
    QList <QGraphicsItem*> items;
    QList<QImage> images;
    QList <QGraphicsItem*> imageItems;
    QList<QGraphicsItem*> buttonItems;
    qreal y = 30;
   // QList<qreal> imageYcoordinats;
    //QList<int> indexesOfTextEdit;
    QProgressBar* prBar;
    QList<QImage> defaultImages;
    QList<WordData> wordsData;

};

#endif // TEXTWIZARD_H
