#include "mainwindow.h"
#include "OCR.h"
#include <QString>
#include <QWidget>
#include <QDialog>
#include <QFileDialog>
#include <QApplication>
#include <QImage>
#include <math.h>
using namespace std;
using namespace cv;


void OCR::recognize(const QStringList& str)
{
if(!str.empty()){
    int progress=0;
    setIPath(str);
    QStringList path =  iPath();
    tesseract::TessBaseAPI *ocr = new tesseract::TessBaseAPI();
    ocr->Init(nullptr, "rus", tesseract::OEM_LSTM_ONLY);
    ocr->SetVariable("preserve_interword_spaces","1");
    ocr->SetVariable("tessedit_pageseg_mode", "6");
    QStringList textes;
    QStringList TSV;

    if (path[0].contains(".pdf"))
    {
        int sepCount = path[0].count(QDir::separator());
        QString pdfPath = path[0].section(QDir::separator(),0,sepCount-1);
        Poppler::Document* document = Poppler::Document::load(pdfPath);
        for (int i = 0; i < path.size(); ++i) {
        int pageNum = path[i].section(QDir::separator(),sepCount,sepCount).toInt();
        Poppler::Page* pdfPage = document->page(pageNum);
        QImage pdfimage = pdfPage->renderToImage(288,288,-1,-1,-1,-1,Poppler::Page::Rotate0);
        Mat image(pdfimage.height(),pdfimage.width(),CV_8UC4,const_cast<uchar*>(pdfimage.bits()),static_cast<size_t>(pdfimage.bytesPerLine()));
        doPerspect(image,image);
        cvtColor(  image, image, COLOR_BGR2GRAY );
        int const max_binary_value = 255;
        ocr->SetImage(image.data, image.cols, image.rows, 1, image.step);       

        const QString outtext = QString::fromStdString(string(ocr->GetUTF8Text()));
        QString tsvtext = QString::fromStdString(string(ocr->GetTSVText(0)));
        TSV.push_back(tsvtext);
        textes.push_back(outtext);
        progress+= 100/str.size();
        emit sendProgress(progress);
       }

    }
    else {
    for (int i =0;i<path.size();i++) {
    string imPath = path[i].toUtf8().constData();
    Mat image = imread(imPath,IMREAD_COLOR);
    doPerspect(image,image);
    cvtColor(  image, image, COLOR_BGR2GRAY );
    int const max_binary_value = 255;
    medianBlur(image,image,3);
    adaptiveThreshold(image,image,max_binary_value,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY,15,20);
    ocr->SetImage(image.data, image.cols, image.rows, 1, image.step);
//    namedWindow("win",WINDOW_NORMAL);
//    imshow("win",image);
//    waitKey(0);
    const QString outtext = QString::fromStdString(string(ocr->GetUTF8Text()));
    QString tsvtext = QString::fromStdString(string(ocr->GetTSVText(0)));  
    TSV.push_back(tsvtext);
    textes.push_back(outtext);
    progress+= 100/str.size();
    emit sendProgress(progress);
    }
  }
    TSVTextes = TSV;
    setOutText(textes);
    emit recognizeEnd(str);
    ocr->End();
}
    }


QString OCR::outTextatindex(const int index)
{
    return m_outText[index];
}

QList<Mat> OCR::segmentation(Mat image)
{

    Mat ImageGr;
    cvtColor(image,ImageGr,COLOR_BGR2GRAY);
    int threshold_value = 128;
         int threshold_type = 1;
         int  max_binary_value = 255;
    threshold( ImageGr, ImageGr, threshold_value, max_binary_value, threshold_type );
    int horizontal_size = ImageGr.cols / 30;


    // Create structure element for extracting horizontal lines through morphology operations
    Mat horizontalStructure = getStructuringElement(MORPH_RECT, Size(horizontal_size, 1));
    // Apply morphology operations
    Mat ImageHor;
    erode(ImageGr, ImageHor, horizontalStructure, Point(-1, -1));
    dilate(ImageHor,ImageHor, horizontalStructure, Point(-1, -1));
    // Show extracted horizontal lines

    int vertical_size = ImageGr.rows / 35;
    Mat ImageVer;
    // Create structure element for extracting vertical lines through morphology operations
    Mat verticalStructure = getStructuringElement(MORPH_RECT, Size(1, vertical_size));
    // Apply morphology operations
    erode(ImageGr, ImageVer, verticalStructure, Point(-1, -1));
    dilate(ImageVer, ImageVer, verticalStructure, Point(-1, -1));

    double alpha = 0.5;
    double beta = 1.0 - alpha;
    Mat Segment;
    addWeighted(ImageHor,alpha,ImageVer,beta,0.0,Segment,-1);
     threshold_value = 100;
          threshold_type =1;
          max_binary_value = 255;
 threshold( Segment, Segment, threshold_value, max_binary_value, 1);

 /////////////////////////////////////////////
 int erosion_size = 5;
 Mat element = getStructuringElement( MORPH_RECT,
                      Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                      Point( erosion_size, erosion_size ) );
 erode( Segment, Segment, element );
//////////////////////////////////////////
 vector<vector<Point>> contours;
 vector<Vec4i> hierarchy;
 findContours(Segment,contours,hierarchy,RETR_TREE,CHAIN_APPROX_SIMPLE);
 QList<Rect> rect;
 QList<Mat> images;
 ulong j=0;
 for (ulong i = 0; i< contours.size();i ++)
 {
    Rect temp = boundingRect(contours[i]);
    if (temp.height < image.rows*0.95 && temp.width<image.cols *0.95
            && temp.height > image.rows * 0.01 && temp.width > image.cols * 0.01)    {
   rect.push_back(temp);
    Mat cropp= image(rect[j]);
     j++;
    images.push_back(cropp);


   }


  }
 return images;
}

QStringList OCR::iPath() const
{
    return m_iPath;
}

void OCR::setIPath(const QStringList &iPath)
{
    m_iPath = iPath;
}

 QStringList OCR::outText() const
{
    return m_outText;
}

void OCR::setOutText(const  QStringList &outText)
{
    m_outText = outText;
}

OCR::OCR (QObject* pobj) : QObject(pobj){}

QImage OCR::perspectImage(QString imagePath)
{
   Mat inputImg = imread(imagePath.toStdString(),IMREAD_COLOR);
   Mat outputImg;
   Mat dst;


       //double angle = angleOfText(inputImg);
       if (inputImg.rows <inputImg.cols )
       {
       int cols = inputImg.cols-1;
       int rows = inputImg.rows-1;
       Point2f center(cols/2,rows/2);
       Mat rotation = getRotationMatrix2D(center,90,1);
       Rect2f bbox = RotatedRect(Point2f(),Size2f(cols,rows),90).boundingRect2f();
       rotation.at<double>(0,2) += bbox.width/2 - inputImg.cols/2;
       rotation.at<double>(1,2) += bbox.height/2 - inputImg.rows/2;
       warpAffine(inputImg,inputImg,rotation,bbox.size());
        }



    Mat ImgGrey;
       cvtColor( inputImg, ImgGrey, COLOR_BGR2GRAY );
    pyrDown( ImgGrey, ImgGrey, Size( ImgGrey.cols/2, ImgGrey.rows/2 ) );

    int threshold_value = 100;
    int threshold_type = 0;
    int  max_binary_value = 255;
    threshold( ImgGrey, ImgGrey, threshold_value, max_binary_value, threshold_type );

    int morph_elem = 0;
    int morph_size = 5;
    Mat element;
       element = getStructuringElement( morph_elem, Size( 2*morph_size + 1, 2*morph_size+1 ), Point( morph_size, morph_size ) );
      morphologyEx( ImgGrey, ImgGrey, 2, element );
    morph_size = 30;
    element = getStructuringElement( morph_elem, Size( 2*morph_size + 1, 2*morph_size+1 ), Point( morph_size, morph_size ) );
     morphologyEx( ImgGrey, ImgGrey, 3, element );

     vector<Point2f> corners;
     double qualityLevel = 0.2;
     double minDistance = ImgGrey.cols;
     int blockSize = 9, gradientSize = 3;
     bool useHarrisDetector = false;
     double k = 0.04;
     int maxCorners = 4;
     int count = 0;


     while( corners.size() < 4)
     {
     goodFeaturesToTrack( ImgGrey,
                          corners,
                          maxCorners,
                          qualityLevel,
                          minDistance,
                          Mat(),
                          blockSize,
                          gradientSize,
                          useHarrisDetector,
                          k );
     minDistance /= 1.35;
     count++;
     if (count > 5)
     {
         corners.clear();
         corners.push_back(Point2f( 0,0 )/2);
         corners.push_back(Point2f( inputImg.cols-1,0)/2);
         corners.push_back(Point2f( inputImg.cols-1,inputImg.rows-1)/2);
         corners.push_back( Point2f (0,inputImg.rows-1)/2 );
         break;
     }



     }



    for (int i=0; i < corners.size(); i++) {
        circle( ImgGrey,
                Point(corners[i].x,corners[i].y),
            30,
            Scalar( 0, 0, 0 ),
            FILLED,
            LINE_8 );

    }





     for(size_t j =0 ; j < corners.size(); j++ )
     {
        for(size_t i =0 ; i < corners.size()-1; i++ )
        {
         float sum1 = corners[i].x + corners[i].y;
         float sum2 = corners[i+1].x + corners[i+1].y;
            if (sum1 > sum2 )
            {
             Point2f temp = corners[i];
             corners[i] = corners[i+1];
             corners[i+1] = temp;
            }
        }
    }

     Point2f inputQuad[4];
     Point2f outputQuad[4];

     Mat lambda = Mat::zeros( inputImg.rows, inputImg.cols, inputImg.type() );

        // The 4 points that select quadilateral on the input , from top-left in clockwise order
        // These four pts are the sides of the rect box used as input
        inputQuad[0] = Point2f( 0,0 );
        inputQuad[1] = Point2f( inputImg.cols-1,0);
        inputQuad[2] = Point2f( inputImg.cols-1,inputImg.rows-1);
        inputQuad[3] = Point2f( 0,inputImg.rows-1  );
        // The 4 points where the mapping is to be done , from top-left in clockwise order
        outputQuad[0] = corners[0]*2;
        outputQuad[1] = corners[1]*2;
        outputQuad[2] = corners[3]*2;
        outputQuad[3] = corners[2]*2;
     lambda = getPerspectiveTransform( outputQuad, inputQuad );

     warpPerspective(inputImg,outputImg,lambda,inputImg.size() );
     cvtColor(outputImg,outputImg,COLOR_BGR2RGB);
     QImage img((const uchar *) outputImg.data, outputImg.cols, outputImg.rows, outputImg.step, QImage::Format_RGB888);
     img.bits();


     return img;
}

QImage OCR::perspectImage(QImage imageqt)
{
    Mat inputImg;

    if (imageqt.format() == QImage::Format_RGB32)
    {   inputImg = Mat(imageqt.height(), imageqt.width(),
                 CV_8UC4, imageqt.bits(), imageqt.bytesPerLine());
    }
    else {
        inputImg = Mat(imageqt.height(), imageqt.width(),
                         CV_8UC3, imageqt.bits(), imageqt.bytesPerLine());
    }
    cvtColor(inputImg, inputImg, COLOR_RGB2BGR);
    Mat outputImg;
    Mat dst;


       // double angle = angleOfText(inputImg);
        if (inputImg.rows <inputImg.cols )
        {
        int cols = inputImg.cols-1;
        int rows = inputImg.rows-1;
        Point2f center(cols/2,rows/2);
        Mat rotation = getRotationMatrix2D(center,90,1);
        Rect2f bbox = RotatedRect(Point2f(),Size2f(cols,rows),90).boundingRect2f();
        rotation.at<double>(0,2) += bbox.width/2 - inputImg.cols/2;
        rotation.at<double>(1,2) += bbox.height/2 - inputImg.rows/2;
        warpAffine(inputImg,inputImg,rotation,bbox.size());
         }

     Mat ImgGrey;
        cvtColor( inputImg, ImgGrey, COLOR_BGR2GRAY );
     pyrDown( ImgGrey, ImgGrey, Size( ImgGrey.cols/2, ImgGrey.rows/2 ) );

     int threshold_value = 100;
     int threshold_type = 0;
     int  max_binary_value = 255;
     threshold( ImgGrey, ImgGrey, threshold_value, max_binary_value, threshold_type );

     int morph_elem = 0;
     int morph_size = 5;
     Mat element;
        element = getStructuringElement( morph_elem, Size( 2*morph_size + 1, 2*morph_size+1 ), Point( morph_size, morph_size ) );
       morphologyEx( ImgGrey, ImgGrey, 2, element );
     morph_size = 30;
     element = getStructuringElement( morph_elem, Size( 2*morph_size + 1, 2*morph_size+1 ), Point( morph_size, morph_size ) );
      morphologyEx( ImgGrey, ImgGrey, 3, element );

      vector<Point2f> corners;
      double qualityLevel = 0.2;
      double minDistance = ImgGrey.cols;
      int blockSize = 9, gradientSize = 3;
      bool useHarrisDetector = false;
      double k = 0.04;
      int maxCorners = 4;
      int count = 0;


      while( corners.size() < 4)
      {
      goodFeaturesToTrack( ImgGrey,
                           corners,
                           maxCorners,
                           qualityLevel,
                           minDistance,
                           Mat(),
                           blockSize,
                           gradientSize,
                           useHarrisDetector,
                           k );
       minDistance /= 1.35;
      count++;
      if (count > 5)
      {
          corners.clear();
          corners.push_back(Point2f( 0,0 )/2);
          corners.push_back(Point2f( inputImg.cols-1,0)/2);
          corners.push_back(Point2f( inputImg.cols-1,inputImg.rows-1)/2);
          corners.push_back( Point2f (0,inputImg.rows-1)/2 );
          break;
      }

      }

      for(size_t j =0 ; j < corners.size(); j++ )
      {
         for(size_t i =0 ; i < corners.size()-1; i++ )
         {
          float sum1 = corners[i].x + corners[i].y;
          float sum2 = corners[i+1].x + corners[i+1].y;
             if (sum1 > sum2 )
             {
              Point2f temp = corners[i];
              corners[i] = corners[i+1];
              corners[i+1] = temp;
             }
         }
     }

      Point2f inputQuad[4];
      Point2f outputQuad[4];

      Mat lambda = Mat::zeros( inputImg.rows, inputImg.cols, inputImg.type() );

         // The 4 points that select quadilateral on the input , from top-left in clockwise order
         // These four pts are the sides of the rect box used as input
         inputQuad[0] = Point2f( 0,0 );
         inputQuad[1] = Point2f( inputImg.cols-1,0);
         inputQuad[2] = Point2f( inputImg.cols-1,inputImg.rows-1);
         inputQuad[3] = Point2f( 0,inputImg.rows-1  );
         // The 4 points where the mapping is to be done , from top-left in clockwise order
         outputQuad[0] = corners[0]*2;
         outputQuad[1] = corners[1]*2;
         outputQuad[2] = corners[3]*2;
         outputQuad[3] = corners[2]*2;
      lambda = getPerspectiveTransform( outputQuad, inputQuad );

      warpPerspective(inputImg,outputImg,lambda,inputImg.size() );
      cvtColor(outputImg,outputImg,COLOR_BGR2RGB);
      QImage img((const uchar *) outputImg.data, outputImg.cols, outputImg.rows, outputImg.step, QImage::Format_RGB888);
      img.bits();
      return img;

}

QStringList OCR::getTSVTextes() const
{
    return TSVTextes;
}

void OCR::setTSVTextes(const QStringList &value)
{
    TSVTextes = value;
}

double OCR::angleOfText(Mat image)
{
    QList<Mat> seg = segmentation(image);
    if(seg.size() == 0)
    {
    int type = image.type();
    int const max_binary_value = 255;
    medianBlur(image,image,3);
     cvtColor( image, image, COLOR_BGR2GRAY );
    adaptiveThreshold(image,image,max_binary_value,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY,15,20);
    Size size = image.size();
    bitwise_not(image, image);
    std::vector<cv::Vec4i> lines;

    int linesize;
    if (size.width > size.height)
    {
        linesize = size.width;
    }
    else {
        linesize = size.height;
    }

    cv::HoughLinesP(image, lines, 1, CV_PI/180,50, linesize / 2, size.height);
    Mat disp_lines(size, CV_8UC1, cv::Scalar(0, 0, 0));
        double angle = 0;
        unsigned nb_lines = lines.size();
        for (unsigned i = 0; i < nb_lines; ++i)
        {
            cv::line(disp_lines, cv::Point(lines[i][0], lines[i][1]),
                     cv::Point(lines[i][2], lines[i][3]), cv::Scalar(255, 0 ,0));
            angle += atan2((double)lines[i][3] - lines[i][1],
                           (double)lines[i][2] - lines[i][0]);
        }
        angle /= nb_lines; // mean angle, in radians.
        namedWindow("win",WINDOW_NORMAL);
//        imshow("win",image);
//        waitKey(0);
//         imshow("win",disp_lines);
//         waitKey(0);
        angle = angle * 180 / CV_PI;

    return angle;
}
    return NAN;
}

void OCR:: doPerspect(Mat &inputImg, Mat &outputImg)
{

    Mat dst;

    if(inputImg.rows < inputImg.cols)
    {
        int cols = inputImg.cols-1;
        int rows = inputImg.rows-1;

        Point2f center(cols/2,rows/2);
        Mat rotation = getRotationMatrix2D(center,90,1);
        Rect2f bbox = RotatedRect(Point2f(),Size2f(cols,rows),90.0).boundingRect2f();
        rotation.at<double>(0,2) += bbox.width/2 - inputImg.cols/2;
        rotation.at<double>(1,2) += bbox.height/2 - inputImg.rows/2;
        warpAffine(inputImg,inputImg,rotation,bbox.size());

    }

    Mat ImgGrey;
    cvtColor( inputImg, ImgGrey, COLOR_BGR2GRAY );
         pyrDown( ImgGrey, ImgGrey, Size( ImgGrey.cols/2, ImgGrey.rows/2 ) );

         int threshold_value = 100;
         int threshold_type = 0;
         int  max_binary_value = 255;
         threshold( ImgGrey, ImgGrey, threshold_value, max_binary_value, threshold_type );

         int morph_elem = 0;
         int morph_size = 5;
         Mat element;
            element = getStructuringElement( morph_elem, Size( 2*morph_size + 1, 2*morph_size+1 ), Point( morph_size, morph_size ) );
           morphologyEx( ImgGrey, ImgGrey, 2, element );
         morph_size = 30;
         element = getStructuringElement( morph_elem, Size( 2*morph_size + 1, 2*morph_size+1 ), Point( morph_size, morph_size ) );
          morphologyEx( ImgGrey, ImgGrey, 3, element );

          vector<Point2f> corners;
          double qualityLevel = 0.2;
          double minDistance = ImgGrey.cols;
          int blockSize = 9, gradientSize = 3;
          bool useHarrisDetector = false;
          double k = 0.04;
          int maxCorners = 4;
          int count = 0;


          while( corners.size() < 4)
          {
          goodFeaturesToTrack( ImgGrey,
                               corners,
                               maxCorners,
                               qualityLevel,
                               minDistance,
                               Mat(),
                               blockSize,
                               gradientSize,
                               useHarrisDetector,
                               k );
           minDistance /= 1.35;
          count++;
          if (count > 5)
          {
              corners.clear();
              corners.push_back(Point2f( 0,0 )/2);
              corners.push_back(Point2f( inputImg.cols-1,0)/2);
              corners.push_back(Point2f( inputImg.cols-1,inputImg.rows-1)/2);
              corners.push_back( Point2f (0,inputImg.rows-1)/2 );
              break;
          }

          }

          for(size_t j =0 ; j < corners.size(); j++ )
          {
             for(size_t i =0 ; i < corners.size()-1; i++ )
             {
              float sum1 = corners[i].x + corners[i].y;
              float sum2 = corners[i+1].x + corners[i+1].y;
                 if (sum1 > sum2 )
                 {
                  Point2f temp = corners[i];
                  corners[i] = corners[i+1];
                  corners[i+1] = temp;
                 }
             }
         }

          Point2f inputQuad[4];
          Point2f outputQuad[4];

          Mat lambda = Mat::zeros( inputImg.rows, inputImg.cols, inputImg.type() );

             // The 4 points that select quadilateral on the input , from top-left in clockwise order
             // These four pts are the sides of the rect box used as input
             inputQuad[0] = Point2f( 0,0 );
             inputQuad[1] = Point2f( inputImg.cols-1,0);
             inputQuad[2] = Point2f( inputImg.cols-1,inputImg.rows-1);
             inputQuad[3] = Point2f( 0,inputImg.rows-1  );
             // The 4 points where the mapping is to be done , from top-left in clockwise order
             outputQuad[0] = corners[0]*2;
             outputQuad[1] = corners[1]*2;
             outputQuad[2] = corners[3]*2;
             outputQuad[3] = corners[2]*2;
          lambda = getPerspectiveTransform( outputQuad, inputQuad );

          warpPerspective(inputImg,outputImg,lambda,inputImg.size() );

          QList<Mat> segments;
          QList<float> angls;
          segments = segmentation(outputImg);
          for (int i = 0; i < segments.size(); ++i) {
              angls.push_back(angleOfText(segments[i]));

          }

          float averageAngle = 0;
          int angleCount = 0;

          for (int i =0;i < angls.size() ;i++ ) {
              if (!isnan(angls[i])) {
                  averageAngle += angls[i];
                 angleCount++;
              }


          }
          averageAngle /= angleCount;

          if(averageAngle > -100 && averageAngle < -80)
          {
              int cols = inputImg.cols-1;
              int rows = inputImg.rows-1;
              Point2f center(cols/2,rows/2);
              Mat rotation = getRotationMatrix2D(center,-90,1);
              Rect2f bbox = RotatedRect(Point2f(),Size2f(cols,rows),-90.0).boundingRect2f();
              rotation.at<double>(0,2) += bbox.width/2 - inputImg.cols/2;
              rotation.at<double>(1,2) += bbox.height/2 - inputImg.rows/2;
              warpAffine(inputImg,inputImg,rotation,bbox.size());

          }



          QImage img((const uchar *) outputImg.data, outputImg.cols, outputImg.rows, outputImg.step, QImage::Format_RGB888);
          img.bits();

 }



