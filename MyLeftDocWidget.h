#ifndef MYLEFTDOCWIDGET_H
#define MYLEFTDOCWIDGET_H
#include <QObject>
#include <QDockWidget>
#include <QEvent>
#include <QResizeEvent>


class MyLeftDocWidget: public QDockWidget
{Q_OBJECT
public:
    MyLeftDocWidget(QWidget* widget);
    void sendSize(QSize size);
signals :
   void docSizeChange(QSize);

protected:
   virtual bool eventFilter(QObject* obj, QEvent* event);


};

#endif // MYLEFTDOCWIDGET_H
